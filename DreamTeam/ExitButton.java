import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ExitButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ExitButton extends MenuActors
{
    /**
     * Act - do whatever the ExitButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public ExitButton()
    {        
    }
    
    public void act() 
    {
        mouseHover();
        if(Greenfoot.mouseClicked(this))
            System.exit(1);
    }    
    
    public void setHoverImage()
    {
        setImage("Menu/MainMenu/ExitButton-hovered.png");
    }
    
    public void setDefaultImage()
    {
        setImage("Menu/MainMenu/ExitButton.png");
    }
}
