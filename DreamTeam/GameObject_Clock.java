import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.Color;  
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Write a description of class Timer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameObject_Clock extends GameObject_Hud
{
    private int minutes = 0;
    private int seconds = 0;
    private long time = 0;
    private Color color;
    private String text;
    /**
     * Act - do whatever the Timer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public GameObject_Clock(String text, Color textColor)
    {
        super(text, 20, textColor, null);  
        this.text = text;
        this.color = textColor;
        this.time = System.currentTimeMillis();       
    }
    
    public void act() 
    {
        long currentTime = System.currentTimeMillis() - time;
            String timeString = String.format(text + "%d:%d", 
                TimeUnit.MILLISECONDS.toMinutes(currentTime),
                TimeUnit.MILLISECONDS.toSeconds(currentTime) - 
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(currentTime))
            );    
        GreenfootImage image = new GreenfootImage(timeString, 20, color, null); 
        setImage(image);
    }        
    
    public long getMinutes (){
        return TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - time);
    }
    public long getSeconds (){
        return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - time);
    }    
}
