import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class UnloadingGame_Crane here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_CraneUpper extends Actor
{
    
    public ShipUnloading_CraneUpper(){
        setImage("Minigame 2/crane_upper.png");
    }
    
    /**
     * Act - do whatever the UnloadingGame_Crane wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
