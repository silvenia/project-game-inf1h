import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.Color;  

/**
 * Write a description of class Hud here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameObject_Hud extends Actor
{    
    protected static final Font font = new Font("Cooper Black", Font.BOLD, 15);  
    
    /**
     * Act - do whatever the Hud wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public GameObject_Hud(int width, int height, Color rgbColor){
        setImage(new GreenfootImage(width, height)); 
        GreenfootImage image = getImage();  
        image.setColor(rgbColor);  
        image.setFont(font);
    }    
    public GameObject_Hud(String text, int height, Color rgbColor, Color bgColor){
        setImage(new GreenfootImage(text, height, rgbColor, bgColor)); 
    }      
}
