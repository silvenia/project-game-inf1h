import greenfoot.*;  

/**
 * The sound class handles the background music for the game.
 * 
 * @author (INF1H-#1) 
 * @version (a version number or a date)
 */
public final class Sound  
{
    private static volatile Sound instance;
  
    GreenfootSound backgroundMusic;
    GreenfootSound menuMusic = new GreenfootSound("Soundtrack - Mario.mp3");    
    GreenfootSound game1Music = new GreenfootSound("Soundtrack - Deep Blue Sea.mp3");
    // heeft nog een soundtrack nodig
    GreenfootSound game2Music = new GreenfootSound("Soundtrack - Reparatie.mp3"); 
    GreenfootSound game3Music = new GreenfootSound("Soundtrack - Reparatie.mp3"); 
    GreenfootSound game4Music = new GreenfootSound("Soundtrack - MI.mp3"); 

    
    public enum MusicTrack{
        MENU,
        GAME1,
        GAME2,
        GAME3,
        GAME4,
        ENDGAME
    }
    
    /**
     * This method used to return the instance of the class. This makes sure only one instance of the sound class is used.
     */
    public static Sound getInstance() {
        if (instance == null) {
            synchronized (Sound.class) {
                if (instance == null) {
                    instance = new Sound();
                }
            }
        }
        return instance;
    }
    
    /**
     * This method sets or changes the backgroundmusic.
     * 
     * @param  music   The music track enum of the music to set as backgroundmusic
     */   
    public void setMusic(MusicTrack music)
    {
        if(backgroundMusic != null)
        {
            backgroundMusic.stop();
        }
        switch(music)
        {
            case MENU:
            {                 
                backgroundMusic = menuMusic;
                backgroundMusic.playLoop();
                break;                
            }
            case GAME1:
            {
                backgroundMusic = game1Music;
                backgroundMusic.playLoop();
                break;
            }
            case GAME2:
            {
                backgroundMusic = game2Music;
                backgroundMusic.playLoop();
                break;
            }
            case GAME3:
            {
                backgroundMusic = game3Music;
                backgroundMusic.playLoop();
                break;
            }
            case GAME4:
            {
                backgroundMusic = game4Music;
                backgroundMusic.playLoop();
                break;
            }            
            
        }
    }
    
    /**
     * This method starts the music if it is not already playing, else ignores.     
     */       
    public void startMusic()
    {
        if(!backgroundMusic.isPlaying())
        {            
            backgroundMusic.playLoop();
        }                                    
    }
    
    /**
     * This method stops the music if it is playing, else ignores.     
     */           
    public void stopMusic()
    {
        if(backgroundMusic.isPlaying())
        {
            backgroundMusic.stop();
        }
    }
    
    /**
     * This method sets the volume of the background music.     
     */           
    public void setVolume(int volume)
    {
        backgroundMusic.setVolume(volume);     
    }       
    
    /**
     * This method check if the music is currently playing.  
     * 
     * @return true/false
     */               
    public boolean isMusicPlaying()
    {
        if(backgroundMusic.isPlaying())
            return true;
        else
            return false;
    }    
}
