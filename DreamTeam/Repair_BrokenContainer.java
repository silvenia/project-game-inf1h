import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Container object: container that needs to be repaired by the player
 */
public class Repair_BrokenContainer extends Actor
{
    public boolean isBroken = true;
    public boolean timerIsMade = false;
    private String toolNeeded;
    private double repairTime;
    public int decider = Greenfoot.getRandomNumber(100);
    boolean isPicked = false;
    boolean canMove;
    private Timer repairTimer;
    public int containersLetThrough = 0;
    private Repair world;

    /**
     * constructor: enables the container to move on spawn
     */
    public Repair_BrokenContainer(){
        canMove = true;
    }

    /**
     * triggers when added to world: calls the setContainerState function
     */
    protected void addedToWorld(World world){
        this.world = (Repair)world;
        setContainerState();
    }
    
    /**
     * changes the images depending on the isBroken state, lets he container move if its not stopped.
     */
    public void act(){        
        if(!isBroken){
            this.setImage("Minigame 3/RepairedContainer.png");
            move();
        }
        if(!isPicked){
            move();
        }
    } 

    /**
     * adds score and funding if the container has been repaired, updates the ContainersLetThrough variable when a container has passed without being repaired
     */
    public void move(){
        move(2);
        if(this.getX() >= world.getWidth() - 5){
            if(this.repairTime == 0){
                if(this.toolNeeded.equals("brush") || this.toolNeeded.equals("wrench")){
                    world.funding.increaseFunds(1000);
                    world.score.addScore(10);
                }

                if(this.toolNeeded.equals("screw") || this.toolNeeded.equals("drill")){
                    world.funding.increaseFunds(1500);
                    world.score.addScore(15);
                }

            }
            if(this.isBroken){
                world.updateContainersLetThrough();
            }
            world.removeObject(this);
        }
    }
    
    /**
     * sets the container to a random state depending on the tools which are bought
     */
    public void setContainerState(){
        if(!world.screwBought && !world.drillBought){
            if(decider <= 50){
                this.toolNeeded = "wrench";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Wrench.png");
            }

            if(decider > 50){
                this.toolNeeded = "brush";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Brush.png");
            }
        }

        if(world.screwBought && !world.drillBought){
            if(decider <= 33){
                this.toolNeeded = "wrench";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Wrench.png");
            }

            if(decider > 33 && decider <= 66){
                this.toolNeeded = "brush";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Brush.png");
            }

            if(decider > 66){
                this.toolNeeded = "screwdriver";
                this.repairTime = 2.5d;
                this.setImage("Minigame 3/Broken_Screw.png");
            }
        }

        if(!world.screwBought && world.drillBought){
            if(decider <= 33){
                this.toolNeeded = "wrench";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Wrench.png");
            }

            if(decider > 33 && decider <= 66){
                this.toolNeeded = "brush";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Brush.png");
            }

            if(decider > 66){
                this.toolNeeded = "drill";
                this.repairTime = 2.5d;
                this.setImage("Minigame 3/Broken_Drill.png");
            }
        }
        if(world.screwBought && world.drillBought){
            if(decider <= 25){
                this.toolNeeded = "wrench";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Wrench.png");
            }

            if(decider > 25 && decider <= 50){
                this.toolNeeded = "brush";
                this.repairTime = 2.0d;
                this.setImage("Minigame 3/Broken_Brush.png");
            }

            if(decider > 50 && decider <= 75){
                this.toolNeeded = "screwdriver";
                this.repairTime = 2.5d;
                this.setImage("Minigame 3/Broken_Screw.png");
            }

            if(decider > 75){
                this.toolNeeded = "drill";
                this.repairTime = 2.5d;
                this.setImage("Minigame 3/Broken_Drill.png");
            }

        }
    }
    
    /**
     * returns the tool that is needed for this container
     */
    public String getToolNeeded(){
        return this.toolNeeded;
    }
    
    /**
     * Shows a clock that visualizes the amount of time that is left needed to repair the container
     */
    public void updateRepairTime(){
        timerIsMade = true;
        final GameObject_CustomTimer timer = new GameObject_CustomTimer(null);
        getWorld().addObject(timer, this.getX(), this.getY() - 50);    
        timer.start(repairTime);
        ActionListener taskPerformer = new ActionListener() 
            {
                public void actionPerformed(ActionEvent evt) {
                    timer.substractTime(0.1d);
                    final double timePercent = repairTime;
                    if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                    {
                        timer.setImage("Timer-4.png");
                    }
                    else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                    {
                        timer.setImage("Timer-3.png");
                    }
                    else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                    {
                        timer.setImage("Timer-2.png");
                    }

                    if(timer.getTimeLeft() <= 0)
                    {   
                        repairTimer.stop();
                        timer.removeTimer();
                        move();
                        isBroken = false;
                        timerIsMade = false;
                        repairTime = 0;

                    }                
                }
            };

        repairTimer = new Timer(100, taskPerformer);   
        repairTimer.setInitialDelay(200);
        repairTimer.start();

    }
    
    /**
     * returns the repairTime that is needed to repair this container
     */
    public double getRepairTime(){
        return this.repairTime;
    }   
    
    /**
     * function that stops the container from moving once it has been stopped by the player
     */
    public void chooseThis(){
        move(0);
        isPicked = true;

        if(canMove){
            this.setLocation(getX(), getY() + 100);
        }
        canMove = false;
    }
}