import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;  
import java.util.*;

/**
 * Write a description of class ShipUnloadingGame_ContainerPosition2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_ContainerPosition extends Actor
{
    int amountOfContainers = 0;
    private GameObject_CustomText text;
    ArrayList<ShipUnloading_Container> containerList = new ArrayList<ShipUnloading_Container>();
    
    public ShipUnloading_ContainerPosition(){
        setImage(new GreenfootImage(10, 10));           
    }
    
    protected void addedToWorld(World world)
    {
        text = new GameObject_CustomText(" " , new Color(0, 0, 0));
        getWorld().addObject(text, getX(), getY());
        spawnContainers();
        getContainersOnTop();        
    }        
    
    public void getContainersOnTop(){
        boolean isEmpty = false;
                        
        List<ShipUnloading_Container> containersOnTop = getIntersectingObjects(ShipUnloading_Container.class);
        for(int i = 0; i < containersOnTop.size(); i++)
        {
            if(containersOnTop.get(i).getCrane() == null)
            {
                amountOfContainers++;
                text.setText(Integer.toString(amountOfContainers));
            }
        }
                
        if(amountOfContainers == 0)
        {
            getWorld().removeObject(text);
            text = null;
        }      
        
    }
    
    private void spawnContainers(){
        for(int i = 0; i < 3; i++){
            ShipUnloading_Container container = new ShipUnloading_Container();
            getWorld().addObject(container, getX(), getY());
            containerList.add(container);
        }               
    }
    
    public void removeContainer(){
        amountOfContainers--;
        text.setText(Integer.toString(amountOfContainers));
 
        if(amountOfContainers == 0)
        {
            getWorld().removeObject(text);
            text = null;
        }      
    }    
    
    public ShipUnloading_Container getTopContainer(){
        if(text == null)
            return null;
                    
        ListIterator li = containerList.listIterator(containerList.size());
        ShipUnloading_Container container = null;
        // Iterate in reverse.
        while(li.hasPrevious()) {            
            container = (ShipUnloading_Container)li.previous();
            if(container.getWorld() != null)
            {               
                break;
            }
            else
                li.remove();            
       }
        
        return container;
    }
    
    public boolean getIntersect(ShipUnloading_ShipHalf shipHalf){
        if(this.intersects(shipHalf))
            return true;
            
        return false;
    }
}
