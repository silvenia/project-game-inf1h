import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.io.*; 
import java.awt.Color;  
/**
 *  Collects highscore data from the save.txt file (save.txt is written by the GameOver Actor) and outputs
 *  it on the highscore menu.
 *  
 *  
 */
public class Highscore extends World
{

    /**
     * sets background and buttons
     * 
     */
    public Highscore()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Menu/MenuBackground.png");
        loadGame();
        MainMenu mainMenu = new MainMenu();
        BackButton backButton = new BackButton(mainMenu);
        backButton.setImage("Menu/BackButton.png");
        addObject(backButton, 640, 620);

    }
    /**
     * loads save.txt, reads the data and outputs it on the screen.
     */
    public void loadGame(){

        try{
            int i = 0;
            BufferedReader br = new BufferedReader(new FileReader("save.txt"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                i++;
                if(i == 8){
                    break;
                }
            }

            String everything = sb.toString();
            GameObject_CustomText customText = new GameObject_CustomText(everything, new Color(0,0,0));
            addObject(customText, 640, 470);
        }
        catch(Exception e){
            System.out.println("There was an error while retrieving the saved data: " + e);
        }

    }
}
