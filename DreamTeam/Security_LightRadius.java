import greenfoot.*;  
import java.awt.Color;
import java.util.*;

/**
 * radius around the player and tower that indicates whether the tower/player can see the burglar
 */
public class Security_LightRadius extends Actor  
{  
    private Actor actor = null;
    private int radiusFlicker = 15;
    private boolean lighten = true;
    private int width, height;
    
    /**
     * constructor: retrieves parameter data
     */
    public Security_LightRadius(Actor actor){
        this.actor = actor;
        if(actor != null)
        {
            this.width = 130;
            this.height = 130;
            setImage(new GreenfootImage(130, 130)); 
        }
        else
        {
            this.width = 200;
            this.height = 200;
            setImage(new GreenfootImage(200, 200));
        }
    }
    
    /**
     * triggers when added to the world: sets color, transparancy and radius depending if its assigned to the player or a tower
     */
    public void addedToWorld(World world)  
    {  
        //Shape circle = new Ellipse2D.Float(150.0f, 150.0f, 150.0f, 150.0f);
        //setImage(new GreenfootImage(getImage().getWidth() / 2, getImage().getHeight() / 2));  
        getImage().setColor(Color.WHITE);
        //getImage().drawShape(circle); // black is default color for new GreenfootImages  
        //getImage().fillShape(circle);
        getImage().setTransparency(55); // for when not done yet  
        if(actor != null)
            getImage().fillOval(0,0,130,130);
        else
            getImage().fillOval(0,0,200,200);
    }  
  
    /**
     * sets the image of the burglar whenever it enters the radius
     * makes the LightRadius flicker in transparancy
     */
    public void act()  
    {  
        List<Security_Burglar> nearBurglar;
        if(actor != null)
            nearBurglar = getObjectsInRange(width - 75, Security_Burglar.class);
        else
            nearBurglar = getObjectsInRange(width - 100, Security_Burglar.class);
        if(!nearBurglar.isEmpty())
        {
            for (int i = 0; i < nearBurglar.size(); i++) {
                nearBurglar.get(i).setVisibility(true);
            }
        }
        
        if(actor != null)
            setLocation(actor.getX(), actor.getY());   
        else{
            if(radiusFlicker > 85)
                lighten = false;
            
            if(radiusFlicker < 5)
                lighten = true;
            if(lighten)
            {           
                radiusFlicker++;
                
            }
            else
                radiusFlicker--;            
            getImage().setTransparency(radiusFlicker);
        }
    }  
    
    /**
     * function used for upgrades
     */
    public void setRadius(int radius){
        this.width = radius;
        this.height = radius;
        setImage(new GreenfootImage(radius, radius)); 
        getImage().setColor(Color.WHITE);
        getImage().setTransparency(55);
        getImage().fillOval(0,0,radius,radius);
        
    }
}  