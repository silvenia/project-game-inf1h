/*
    This is an exapmle of an A* Pathfinding Algorithm written in Java/Greenfoot for my educational purposes and to prototype for future useage in games
    Copyright (C) 2011 Shaun Steenkamp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*; // List, *
import java.awt.Color;

/**
 * Write a description of class pathNode here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Security_PathNode extends Actor
{
    public static final int width = 7;
    public static final int height = 7;
    private GreenfootImage img;
    private int x;
    private int y;
    private List<traversalCost> pathEdges = new ArrayList();
    private Security world;
    
    public void changeColor(Color c) {
        img.clear();
        img.setColor(c);
        img.fillOval(0,0,width,height);
    }
    
    public Security_PathNode(int x, int y) {
        this.x = x;
        this.y = y;
        img = new GreenfootImage(width, height);
        changeColor(Color.black);
        img.setTransparency(0);
        setImage(img);
    }
    
    public Security_PathNode(int x, int y, boolean red) {
        this(x, y);
        if(red) {
            //setImage("red_circle.png");
            changeColor(Color.red);
        }
    }
    
    public void addedToWorld(World world) {
        this.world = (Security) world;
    }
    
    public void act() {
        setStatus();
    }
    
    public void addPathEdge(traversalCost node) {
        pathEdges.add(node);
    }
    
    public List<traversalCost> getPathEdges() {
        return pathEdges;
    }
    
    public int getXcoord() {
        return x;
    }
    public int getYcoord() {
        return y;
    }
    
    /**
     * if the mouse is pressed on this node toggle it's state depending on the key pressed.
     */
    private void setStatus() {
        /*if(Greenfoot.mousePressed(this)) {
            if(Greenfoot.isKeyDown("s")) {
                if(world.start != null) {
                    world.start.changeColor(Color.black);
                }
                world.start = this;
                changeColor(Color.green);
            } else if(Greenfoot.isKeyDown("f")) {
                if(world.finish != null) {
                    world.finish.changeColor(Color.black);
                }
                world.finish = this;
                changeColor(Color.red);
            }
        }*/
    }
}