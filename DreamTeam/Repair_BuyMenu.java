import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class BuyMenu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Repair_BuyMenu extends GameButtons
{
    /**
     * Act - do whatever the BuyMenu wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    private int timesClicked = 0;
    Repair_Tools drill = new Repair_Tools(Repair_Tools.ToolType.DRILL);
    Repair_Tools screwdriver = new Repair_Tools(Repair_Tools.ToolType.SCREWDRIVER);
    
    public Repair_BuyMenu(){
        setImage("Minigame 3/BuyMenu.png");
    }
    
    public void act() {
        if(Greenfoot.mouseClicked(this)){
            userInteraction();
        }
    }    
    
    public void userInteraction(){
        timesClicked++;
        Repair world = (Repair) getWorld();
            
        if(timesClicked % 2 == 1){
            Greenfoot.setSpeed(25);
            if(!drill.isBought()){
                world.addObject(drill, this.getX(), this.getY() + 70);
            }
            
            if(!screwdriver.isBought()){
                world.addObject(screwdriver, this.getX(), this.getY() + 170);
            }
            
        }
        
        if(timesClicked % 2 == 0) {
            Greenfoot.setSpeed(50);
            world.removeObject(drill);
            world.removeObject(screwdriver);
        }
    }
}
