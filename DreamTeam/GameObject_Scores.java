import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.Color;  

/**
 * Write a description of class Scores here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameObject_Scores extends GameObject_Hud
{
    private int score = 0;
    
    /**
     * Act - do whatever the Scores wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */    
    
    public GameObject_Scores(Color textColor)
    {        
        super(120, 20, textColor);
        updateScore();
    }
    
    public int getScore(){
        return score;
    }

    public void addScore(final int score){        
        this.score += score;
        updateScore();
    }
    
    private void updateScore()
    {
        GreenfootImage image = getImage();  
        image.clear(); 
        image.drawString("Score: " + score, 1, 16); 
    }
}
