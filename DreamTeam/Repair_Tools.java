import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Tool object: one actor that contains all four tools
 */

public class Repair_Tools extends Actor
{
    private boolean useable = false;
    public boolean bought = false;
    private ToolType toolType;
    private Repair world;
    
    /**
     * Different enums for different tools
     */
    public  enum ToolType{
        DRILL,
        SCREWDRIVER,
        WRENCH,
        BRUSH
    }
    
    /**
     * sets image to itself depending on the given enum
     */
    public Repair_Tools(ToolType toolType){
        this.toolType = toolType;
        switch(toolType){
            case DRILL:
            setImage("Minigame 3/Drill.png");
            break;
            case SCREWDRIVER:
            setImage("Minigame 3/Screwdriver.png");
            break;
            case WRENCH:
            this.useable = true;
            setImage("Minigame 3/Wrench.png");
            break;
            case BRUSH:
            this.useable = true;
            setImage("Minigame 3/Brush.png");
            break;

        }

    }
    
    /**
     * sets world variable to the repair world when this actor is added to the world
     */
    protected void addedToWorld(World world){
        this.world = (Repair)world;
    
    }

    /**
     * calls the buy function whenever there's an attempt to buy the tool
     */
    public void act() 
    {
        if(toolType == ToolType.DRILL || toolType == ToolType.SCREWDRIVER){
            if(!bought && Greenfoot.mouseClicked(this)){
                buy();
            }
        }

    }
    
    /**
     * checks if the player has enough money, if so makes the tool accessible and decrease funds.
     */
    public void buy(){
        if(world.funding.getFunds() >= 7000){
            bought = true;
            Repair_Tools tool = new Repair_Tools(getType());
            tool.setUsable(true);
            if(getType() == toolType.DRILL){
                world.addObject(tool,250, 627);
                world.drillBought = true;
            }
            else{
                world.addObject(tool,350, 627);
                world.screwBought = true;
            }
            world.funding.decreaseFunds(7000);
        }
    } 
    
    /**
     * returns whether the tools is bought and thus useable
     */
    public boolean getUseable(){
        return useable;
    
    }
    
    /**
     * sets the useable variable
     */
    public void setUsable(boolean useable){
        this.useable = useable;
    }
    
    public ToolType getType(){
        return toolType;
    }
    
    public boolean isBought(){
        return bought;    
    }
    
}
   

