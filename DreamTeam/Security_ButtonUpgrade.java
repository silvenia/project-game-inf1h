import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Whenever the Player clicks this button a upgrade is bought.
 */
public class Security_ButtonUpgrade extends Actor
{
    private int upgradeType;
    private int funds;
    
    /**
     * Constructor: retrieves parameter data
     */
    public Security_ButtonUpgrade(int type){
        this.upgradeType = type;
    
    }
    
    
    /**
     * calls the function clickCheck
     * 
     */
    public void act() 
    {
        clickCheck();
    }    
    
    /**
     * whenever the button has been clicked do the following:
     * - check which upgrade has been clicked
     * - remove the amount of money
     * - upgrade 
     */
    public void clickCheck(){
        if(Greenfoot.mouseClicked(this)){
            Security security = (Security)getWorld();
            funds = security.getFunds();
            switch(upgradeType){
            case 1: 
                if(funds >= 1000){
                    security.increaseSpeed();
                    security.decreaseFunds(1000);
                    getWorld().removeObject(this);
                }
                break;
            
            case 2: 
                if(funds >= 500){
                    security.updateTowerRadius();
                    security.decreaseFunds(500);
                    getWorld().removeObject(this);
                }
                break;
            
            case 3: 
                if(funds >= 750){
                    security.updatePlayerRadius();
                    security.decreaseFunds(750);
                    getWorld().removeObject(this);
                }
                break;
            
            
            }
        }
    
    }
}
