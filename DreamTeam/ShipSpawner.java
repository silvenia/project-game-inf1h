import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
/**
 * Write a description of class ShipSpawner here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipSpawner  
{       
    private Timer spawnTimer;
    private ControlCentrum_SpawnPoint spawnPoint;
    /**
     * Constructor for objects of class ShipSpawner
     */
    public ShipSpawner(final ControlCentrum_SpawnPoint spawnPoint)
    {
        this.spawnPoint = spawnPoint;
    }
    
    public boolean isSpawning()
    {
        if(spawnTimer == null)
            return false;
            
        return true;
    }
    
    public void spawnShip(final ControlCentrum c)
    {      
        if(spawnTimer != null)
            return;

        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {

                //50% spawn chance of oil and container type ships
                int shipDecider = Greenfoot.getRandomNumber(10);
                int shipSize = Greenfoot.getRandomNumber(15);                
                int repairDecider = Greenfoot.getRandomNumber(10);
                
                int repairTime = 0;
                boolean canSpawn = true;
                Enum.ShipType type = null;
                Enum.ShipState shipState = Enum.ShipState.UNDAMAGED;  
                
                if(shipDecider < 5){      
                    if(shipSize <= 5)
                        type = Enum.ShipType.OIL_SMALL;
                    else if(shipSize <= 10)
                        type = Enum.ShipType.OIL_MEDIUM;
                    else
                        type = Enum.ShipType.OIL_LARGE;
                        
                }
                else if(shipDecider >= 5){
                    if(shipSize <= 5)
                        type = Enum.ShipType.CONTAINER_SMALL;
                    else if(shipSize <= 10)
                        type = Enum.ShipType.CONTAINER_MEDIUM;
                    else 
                        type = Enum.ShipType.CONTAINER_LARGE;
                }
        
                //30% chance for a spawned ship to be damaged
                if(repairDecider < 3){
                    repairTime = 100;
                    shipState = Enum.ShipState.DAMAGED;
                }            
                else if(repairDecider >= 3){
                    repairTime = 0;
                    shipState = Enum.ShipState.UNDAMAGED;
                }
    
                List<ControlCentrum_SpawnPoint> spawn = c.getObjects(ControlCentrum_SpawnPoint.class);  
                if(spawn.size() > 0)  
                {
                    for(ControlCentrum_SpawnPoint SP : spawn)  
                    {  
                        if(!SP.canSpawn())
                            canSpawn = false;
                    }  
                    
                }
                if(canSpawn)
                    c.addObject(new ControlCentrum_Ship(type, shipState), spawnPoint.getX(), spawnPoint.getY());               
            }
        };
        
        spawnTimer = new Timer(15000, taskPerformer);   
        spawnTimer.setInitialDelay(2000);
        spawnTimer.start();
    }
    
    /* 
     * Set speed between ship spawns
     * To be used to increase difficulty     
     */
    public void setSpawnSpeed(int speed)
    {
        if(spawnTimer == null)
            return;
            
        spawnTimer.setDelay(speed);
    }    
}
