import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Player object that the actual player controls
 * 
 */
public class Security_Player extends SmoothMover
{
    private Security world;
    final SoundEffect sound = new SoundEffect();
    private int animationTime = 0;
    private boolean animatePlayer = false;    
    GreenfootImage playerUp;
    GreenfootImage playerDown;
    GreenfootImage playerLeft;
    GreenfootImage playerRight;
    private boolean test = false;
    private Enum.Direction direction;
    private Security_Burglar arrestingBurglar;
    private Timer arrestTimer;
    private float speed = 1.5f;
    
    /**
     * Constructor: sets spawnimage and inializes different images
     */
    public Security_Player()
    {
        setImage("images/Minigame 4/Player-right.png");
        playerUp = new GreenfootImage("images/Minigame 4/Player-up.png");
        playerDown = new GreenfootImage("images/Minigame 4/Player-down.png");
        playerLeft = new GreenfootImage("images/Minigame 4/Player-left.png");
        playerRight = new GreenfootImage("images/Minigame 4/Player-right.png");
        
    }
    
    public void addedToWorld(World world) {
        this.world = (Security) world;             
    }
    
    /**
     * - arrest a burglar when it gets within the player's radius
     * - move animation
     */
    public void act() 
    {
        int oldX = getX();
        int oldY = getY();
        
        if(arrestTimer == null)
        {
            List<Security_Burglar> nearBurglar = getObjectsInRange(60, Security_Burglar.class);
            if(nearBurglar != null){
                for(int z = 0; z < nearBurglar.size(); z++)
                {
                    arrestingBurglar = nearBurglar.get(z);
                    if(!nearBurglar.get(z).isBeingArrested())
                    {
                        nearBurglar.get(z).arrest(true);
                        arrest();
                        break;
                    }                    
                }
            
            }else
            {
                if(arrestingBurglar != null)
                {
                    arrestingBurglar.arrest(false);
                    sound.stopEffect(); 
                }
            }
        }
        
        movePlayer();
        if((getOneIntersectingObject(Security_Building.class) != null) || 
        (getOneIntersectingObject(Security_Container.class) != null) || 
        (getOneIntersectingObject(Security_Fence.class) != null)
        )
        {
            setLocation(oldX, oldY);
        }

        if(animatePlayer)
        {
            switch(getRotation())
            {
                case 0:
                {   
                    if(animationTime > 30)
                    {
                        setImage(playerRight);
                        animationTime = 0;
                    }
                    else if(animationTime > 20)
                        setImage("images/Minigame 4/Player-right2.png");
                    else if(animationTime > 10)
                        setImage("images/Minigame 4/Player-right1.png");
                    break;
                }
                case 90:
                {
                    if(animationTime > 30)
                    {
                        setImage(playerDown);
                        animationTime = 0;
                    }
                    else if(animationTime > 20)
                        setImage("images/Minigame 4/Player-down2.png");
                    else if(animationTime > 10)
                        setImage("images/Minigame 4/Player-down1.png");
                    break;
                }
                case 180:
                {
                    if(animationTime > 30)
                    {
                        setImage(playerLeft);
                        animationTime = 0;
                    }
                    else if(animationTime > 20)
                        setImage("images/Minigame 4/Player-left2.png");
                    else if(animationTime > 10)
                        setImage("images/Minigame 4/Player-left1.png");
                    break;
                }      
                case 270:
                {
                    if(animationTime > 30)
                    {
                        setImage(playerUp);
                        animationTime = 0;
                    }
                    else if(animationTime > 20)
                        setImage("images/Minigame 4/Player-up2.png");
                    else if(animationTime > 10)
                        setImage("images/Minigame 4/Player-up1.png");
                    break;
                }                    
            }
            animationTime++;
        }
    }   
    
    /**
     * makes the player able to move around by using the arrows keys, images change depending on the animation state
     */
    public void movePlayer()
    {              
        if(Greenfoot.isKeyDown("up"))
        {
            if(getImage() != playerUp)
                setImage(playerUp);            
            setRotation(270);
            move(speed);
            animatePlayer = true;
            setDirection(Enum.Direction.NORTH);
            
        }
        else if(Greenfoot.isKeyDown("down"))
        {
            if(getImage() != playerDown)
                setImage(playerDown);            
            setRotation(90);
            move(speed);
            animatePlayer = true;
            setDirection(Enum.Direction.SOUTH);            
        }        
        else if(Greenfoot.isKeyDown("left"))
        {
            if(getImage() != playerLeft)
                setImage(playerLeft);
 
            setRotation(180);
            move(speed);
            animatePlayer = true;
            setDirection(Enum.Direction.WEST);
        }
        else if(Greenfoot.isKeyDown("right"))
        {
            if(getImage() != playerRight)
                setImage(playerRight);
            setRotation(0);
            move(speed);
            animatePlayer = true;
            setDirection(Enum.Direction.EAST);
        }  
        else
        {       
            switch(getRotation())
            {
                case 0:
                {
                    setImage(playerRight);
                    break;
                }
                case 90:
                {
                    setImage(playerDown);
                    break;
                }
                case 180:
                {
                    setImage(playerLeft);
                    break;
                }
                case 270:
                {
                    setImage(playerUp);
                    break;
                }                
            }                
            animationTime = 0;
            animatePlayer = false;
        }
    }
    
    public Enum.Direction getDirection(){
        return this.direction;
    }
    
    public void setDirection(Enum.Direction direction){
        this.direction = direction;
    }
    
    /**
     * actual arrest function which starts a arrest timer
     * once the timer ends adds score, funds and plays a sound
     */
    public void arrest (){
        if(arrestTimer != null)
            return;
        final GameObject_CustomTimer timer = new GameObject_CustomTimer(this);
        getWorld().addObject(timer, getX(), getY() - 50);    
        timer.start(2.0d); 
        sound.playEffect(SoundEffect.Effect.ARREST, false);  
        ActionListener taskPerformer = new ActionListener() 
            {
                public void actionPerformed(ActionEvent evt) {
                    timer.substractTime(0.1d);
                    final double timePercent = 2.0d;
                    List<Security_Burglar> nearBurglar = getObjectsInRange(60, Security_Burglar.class);
                    boolean outOfRange = true;
                    for(int z = 0; z < nearBurglar.size(); z++)
                    {
                        if(nearBurglar.get(z) == arrestingBurglar)
                        {
                            outOfRange = false;
                            break;
                        }
                    }
                    if(outOfRange){
                        arrestTimer.stop();
                        timer.removeTimer();
                        arrestTimer = null;
                        arrestingBurglar.arrest(false);
                        sound.stopEffect(); 
                    }
                    if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                    {
                        timer.setImage("Timer-4.png");
                                           }
                    else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                    {
                        timer.setImage("Timer-3.png");
                    }
                    else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                    {
                        timer.setImage("Timer-2.png");
                    }
                    
                    if(timer.getTimeLeft() <= 0)
                    {   
                        arrestTimer.stop();
                        timer.removeTimer();
                        if(arrestingBurglar != null)
                        {
                            getWorld().removeObject(arrestingBurglar);
                            arrestingBurglar = null;
                        }
                        
                        Security security = (Security)getWorld();
                        security.score.addScore(10);
                        security.addSatisfaction(2);
                        security.increaseFunds(50);
                        arrestTimer = null;
                        sound.stopEffect();                                           
                    }                
                }
            };
            
            arrestTimer = new Timer(100, taskPerformer);   
            arrestTimer.setInitialDelay(200);
            arrestTimer.start();
    
    }
    
    /**
     *  increases the player's speed, used after an upgrade has been bought
     */
    public void increaseSpeed(){
        speed++;
    }    
}
