import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  

/**
 * Write a description of class Scores here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameObject_TextFloat extends GameObject_Hud
{
    private String floatingText;
    private int floatTime = 0;
    private int count = 0;
    /**
     * Act - do whatever the Scores wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */    
    
    public GameObject_TextFloat(String text, Color rgbColor)
    {        
        super(text , 24, rgbColor, null);
        this.floatingText = text;    
        SoundEffect sound = new SoundEffect();
        sound.playEffect(SoundEffect.Effect.POP, true);
    }
    public void act()
    {
        if(count > 80)
            getWorld().removeObject(this);
        if(floatTime >= 2)
        {
            if(count < 40)
                setLocation(getX(), getY()-1);
            floatTime = 0;
            count++;
        }
        
        floatTime++;
    }    

}
