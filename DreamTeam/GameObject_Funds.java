import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color; 
/**
 * Write a description of class Funds here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameObject_Funds extends GameObject_Hud
{
    private int funds = 0;
    /**
     * Act - do whatever the Funds wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public GameObject_Funds( Color textColor)
    {        
        super(125, 20, textColor);
        updateFunds();
    }
    
    private void updateFunds()
    {
        GreenfootImage image = getImage();  
        image.clear(); 
        image.drawString("Funds: " + funds, 1, 16); 
    }
    
    public int getFunds()
    {
        return funds;
    }
    
    public void decreaseFunds(int funds)
    {
        this.funds -= funds;
        if(this.funds < 0)
            this.funds = 0;
            
        updateFunds();
    }
    public void increaseFunds(int funds)
    {
        this.funds += funds;
        updateFunds();
    }    
}
