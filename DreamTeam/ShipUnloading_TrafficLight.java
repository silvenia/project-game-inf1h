import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShipUnloading_TrafficLight here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_TrafficLight extends Actor
{
    public enum TrafficColor{
        RED,
        GREEN
    }
    
    private int waitTime = 0;
    TrafficColor color;
    TrafficColor nextColor;
    
    
    public ShipUnloading_TrafficLight(){
        setTrafficLight(TrafficColor.RED);
    }
    
    /**
     * Act - do whatever the ShipUnloading_TrafficLight wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() {
        if(waitTime > 0)
        {
            waitTime--;
        }
        
        if(waitTime == 0 && nextColor != null){
            setTrafficLight(nextColor);
            nextColor = null;
            waitTime = 0;
        }
            
        if(Greenfoot.mouseClicked(this) && getX() < 700 && waitTime <= 0){
            if(getColor() == TrafficColor.RED){
                setColor(TrafficColor.GREEN);
            }
            else
                 setColor(TrafficColor.RED);
        }
    }    
    
    public TrafficColor getColor(){
        return color;
    }
    
    public void setColor(TrafficColor color){
        waitTime = 90;
        this.nextColor = color;
    }    
    public void setTrafficLight(TrafficColor color){
        this.color = color;
        if(color == TrafficColor.RED)
            setImage("Minigame 2/red-light.png");
        else if(color == TrafficColor.GREEN)
            setImage("Minigame 2/green-light.png");
    }
    
    public int getWaitTime(){
        return waitTime;
    }
}
