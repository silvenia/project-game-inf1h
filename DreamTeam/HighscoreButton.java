import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class HighscoreButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class HighscoreButton extends MenuActors
{
    /**
     * Act - do whatever the HighscoreButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        mouseHover();
        if(Greenfoot.mouseClicked(this))
        {
            World nextWorld = new Highscore(); // create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld); // create curtain object  
            int x = getWorld().getWidth()/2;  
            int y = getWorld().getHeight()/2;  
            getWorld().addObject(fader, x, y); // add curtain into world              
        }  
    }    

    public void setHoverImage()
    {
        setImage("Menu/MainMenu/HighscoreButton-hovered.png");
    }

    public void setDefaultImage()
    {
        setImage("Menu/MainMenu/HighscoreButton.png");
    }
}
