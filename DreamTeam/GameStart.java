import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * shows the player guide to the game
 * 
 */
public class GameStart extends World
{
    /**
     * different Enums for the different types of games
     */
    public enum GameType{
        CONTROLCENTRUM,
        SHIPUNLOADING,
        REPAIR,
        SECURITY
    }
    /**
     * Constructor: checks which button is pressed and shows the corrosponding player guide
     * 
     */
    public GameStart(GameType gameType)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        PlayButton play = null;
        switch(gameType){
            case CONTROLCENTRUM:
                setBackground("Images/Menu/Game1Start.png");
                play = new PlayButton(new ControlCentrum());
                break;
            case SHIPUNLOADING:
                setBackground("Images/Menu/Game2Start.png");
                play = new PlayButton(new ShipUnloading());
                break;
            case REPAIR:
                setBackground("Images/Menu/Game3Start.png");
                play = new PlayButton(new Repair());
                break;
            case SECURITY:
                setBackground("Images/Menu/Game4Start.png");
                play = new PlayButton(new Security());
                break;

        
        }
        
        play.setImage("Menu/MainMenu/PlayButton.png");

        addObject(play, 380, 550);
    }

}
