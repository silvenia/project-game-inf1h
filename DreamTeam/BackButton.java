import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class BackButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class BackButton extends MenuActors
{
    /**
     * Act - do whatever the BackButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */   
    World world;
    
    public BackButton(final World world)
    {
        this.world = world;
    }
    public void act() 
    {
        mouseHover();
        if(Greenfoot.mouseClicked(this))
        {
            MenuObject_Fader fader = new MenuObject_Fader(world); // create curtain object  
            int x = getWorld().getWidth()/2;  
            int y = getWorld().getHeight()/2;  
            getWorld().addObject(fader, x, y); // add curtain into world  
        }
    }    
    
    
    public void setHoverImage()
    {
        setImage("Menu/BackButton-hovered.png");
    }
    
    public void setDefaultImage()
    {
        setImage("Menu/BackButton.png");
    }    
}
