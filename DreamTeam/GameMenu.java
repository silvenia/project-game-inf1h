import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;  // (List)
import java.awt.image.*; // (BufferedImage)
import java.io.*; // (File)
import javax.imageio.*; // {ImageIO)
    
/**
 * Menu where the player selects which Minigame he wants to play
 */
public class GameMenu extends World
{
    World world;
    
    
    /**
     * Constructor: sets background and adds a fader effect
     * 
     */
    public GameMenu(World world)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Menu/MenuBackground.png");
        this.world = world;
        addObject(new MenuObject_Fader(null), getWidth()/2, getHeight()/2);  
        prepare();
    }
    
    /**
     * adds the buttons to the screen
     */
    private void prepare()
    {
        int centerX = getWidth() / 2;
        int centerY = getHeight() / 2;
        
        // Create buttons and setImage
        MiniGame1Button game1 = new MiniGame1Button();
        game1.setImage(new GreenfootImage("Menu/GameMenu/MiniGame1.png"));
        
        MiniGame2Button game2 = new MiniGame2Button();
        game2.setImage(new GreenfootImage("Menu/GameMenu/MiniGame2.png"));
        
        MiniGame3Button game3 = new MiniGame3Button();
        game3.setImage(new GreenfootImage("Menu/GameMenu/MiniGame3.png"));
        
        MiniGame4Button game4 = new MiniGame4Button();
        game4.setImage(new GreenfootImage("Menu/GameMenu/MiniGame4.png"));
        
        BackButton back = new BackButton(world);
        back.setImage(new GreenfootImage("Menu/BackButton.png"));
        
        // Add buttons and position them into the world ( MainMenu )
        addObject(game1, centerX, centerY + 15);
        addObject(game2, centerX, centerY + 75);
        addObject(game3, centerX, centerY + 135);
        addObject(game4, centerX, centerY + 195);
        addObject(back, centerX, centerY + 255);
        
        
    }
}
