import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Shows the credit screen after is has been selected from the main menu
 * 
 */
public class CreditsScreen extends World
{

    /**
     * Constructor: sets background and image
     * 
     */
    
    public CreditsScreen(World world)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Menu/Credits.png");
        
        BackButton back = new BackButton(world);
        back.setImage(new GreenfootImage("Menu/BackButton.png"));
        addObject(back, 700, 525);
    }
}
