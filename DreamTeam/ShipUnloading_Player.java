import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.awt.Color;  

/**
 * Write a description of class ExampleActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_Player  extends ShipUnloading_CranePlay{     
    private boolean hasContainer = false;
    private boolean isBusy = false;
    private ShipUnloading_Container pickedUp;

    public ShipUnloading_Player(){
        setImage("Minigame 2/crane_player.png");
        setSequences(new Sequence[] { new MoveSequence(), new ContainerSequence()});
    }

    protected void addedToWorld(World world) {
        this.world = (ShipUnloading) world;     
        craneUpper = new ShipUnloading_CraneUpper();
        getWorld().addObject(craneUpper, 410, 455);       
    }

    class MoveSequence extends Sequence{
        public void doRun() throws InterruptedException{        
            while (true) {   
                if(!isBusy){    
                    int oldX = getX();
                    int oldY = getY();

                    if(Greenfoot.isKeyDown("left")){
                        setLocation(getX() - 2, getY());
                    }        
                    else if(Greenfoot.isKeyDown("right")){
                        setLocation(getX() + 2, getY());
                    }

                    if(Greenfoot.isKeyDown("up")){
                        craneUpper.setLocation(craneUpper.getX(), getY() - 2);
                        setLocation(getX(), getY() - 2);
                    }     
                    else if(Greenfoot.isKeyDown("down")){
                        craneUpper.setLocation(craneUpper.getX(), getY() + 2);
                        setLocation(getX(), getY() + 2);
                    }   

                    if(getY() < 270 || getY() > 778)
                        setLocation(getX(), oldY);

                    if(getX() < 70 || getX() > 750)
                        setLocation(oldX, getY());        
                }
                waitForNextSequence();
            }
        }
    }
    class ContainerSequence extends Sequence{
        public void doRun() throws InterruptedException{           
            while (true) {
                if(!isBusy){
                    if(Greenfoot.getKey() == "space"){                        
                        if(pickedUp == null){
                            ShipUnloading_ContainerPosition topContainer = (ShipUnloading_ContainerPosition)getOneIntersectingObject(ShipUnloading_ContainerPosition.class);
                            if(topContainer != null){                
                                ShipUnloading_Container container;
                                if((container = topContainer.getTopContainer()) != null)
                                {
                                    pickedUp = container;
                                    grabContainer(container);
                                    isBusy = true; 
                                }
                            }
                        }
                        else{
                            ShipUnloading_Truck truck = (ShipUnloading_Truck)getOneIntersectingObject(ShipUnloading_Truck.class);
                            ShipUnloading_Train train = (ShipUnloading_Train)getOneIntersectingObject(ShipUnloading_Train.class);                 
                            if(truck != null && !truck.isLoaded() && truck.getSpeed() == 0){
                                if(pickedUp.getWeight() != 350){
                                    GameObject_TextFloat test = new GameObject_TextFloat("Container too heavy!", new Color(0, 191, 255));
                                    getWorld().addObject(test, getX(), getY());
                                }
                                else{
                                    isBusy = true;  
                                    placeContainer(truck);
                                }
                            }
                            else if(train != null && !train.isLoaded() && train.getSpeed() == 0){
                                isBusy = true;  
                                placeContainer(train);
                            }
                        }
                    }   
                }
                waitForNextSequence();
            }
        }
    }    
    public void grabContainer(final ShipUnloading_Container grabbedContainer){
        final GameObject_CustomTimer textTimer = new GameObject_CustomTimer(null);
        final Timer timer = new Timer();
        getWorld().addObject(textTimer, getX(), getY() - 50);  
        textTimer.start(3.0d);
        timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    textTimer.substractTime(0.1d);
                    final double timePercent = 3.0d;
                    if(textTimer.getTimeLeft() < ( timePercent * 0.1 )){
                        textTimer.setImage("Timer-4.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.5 )){
                        textTimer.setImage("Timer-3.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.75 )){
                        textTimer.setImage("Timer-2.png");
                    }

                    if(textTimer.getTimeLeft() <= 0){                          
                        textTimer.removeTimer();                     
                        isBusy = false; 
                        grabbedContainer.setCrane(getParent());
                        world.calculatePlayerWeight();
                        timer.cancel();
                    }     
                }
            }, 100, 100);      
    }

    private void placeContainer(final ShipUnloading_Truck truck){
        final GameObject_CustomTimer textTimer = new GameObject_CustomTimer(null);
        final Timer timer = new Timer();
        getWorld().addObject(textTimer, getX(), getY() - 50);  
        textTimer.start(2.0d);
        timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    textTimer.substractTime(0.1d);
                    final double timePercent = 2.0d;
                    if(textTimer.getTimeLeft() < ( timePercent * 0.1 ))
                    {
                        textTimer.setImage("Timer-4.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.5 ))
                    {
                        textTimer.setImage("Timer-3.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.75 ))
                    {
                        textTimer.setImage("Timer-2.png");
                    }

                    if(textTimer.getTimeLeft() <= 0)
                    {                          
                        textTimer.removeTimer();                     
                        isBusy = false;          
                        truck.setLoaded(true);
                        getWorld().removeObject(pickedUp);
                        pickedUp = null;
                        timer.cancel();
                    }     
                }
            }, 100, 100);         
    }

    private void placeContainer(final ShipUnloading_Train train){
        final GameObject_CustomTimer textTimer = new GameObject_CustomTimer(null);
        final Timer timer = new Timer();
        getWorld().addObject(textTimer, getX(), getY() - 50);  
        textTimer.start(2.0d);
        timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    textTimer.substractTime(0.1d);
                    final double timePercent = 2.0d;
                    if(textTimer.getTimeLeft() < ( timePercent * 0.1 ))
                    {
                        textTimer.setImage("Timer-4.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.5 ))
                    {
                        textTimer.setImage("Timer-3.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.75 ))
                    {
                        textTimer.setImage("Timer-2.png");
                    }

                    if(textTimer.getTimeLeft() <= 0)
                    {                          
                        textTimer.removeTimer();                     
                        isBusy = false;          
                        train.loadContainer();
                        getWorld().removeObject(pickedUp);
                        pickedUp = null;
                        timer.cancel();
                    }     
                }
            }, 100, 100);         
    }    

    public ShipUnloading_Player getParent(){
        return this;
    }    
}
