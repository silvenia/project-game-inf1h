import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.awt.*;  
import java.awt.geom.Rectangle2D;  
import java.awt.font.FontRenderContext; 
import java.io.*; 

/**
 * Minigame 1: ControlCentrum
 * 
 */

public class ControlCentrum extends World
{
    public Sound track = Sound.getInstance();
    private ControlCentrum_Ship ship;
    private int counter = 0;
    private int repairTime;
    private String type;   
    public int funds = 0;
    private int scored;
    public GameObject_Scores score = new GameObject_Scores(new Color(0,0,0));
    public GameObject_Funds funding = new GameObject_Funds(new Color(0,0,0));
    private ControlCentrum_SelectionBox selection = null;
    private int shipsLetThrough = 0;

    /**
     * Constructor: sets background, calls the prepare function and sets the music
     * 
     */
    public ControlCentrum()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Minigame 1/Map.png");
        prepare();        

        //track.stopMainMenuSoundTrack();
        track.setMusic(Sound.MusicTrack.GAME1);

        //spawner = ShipSpawner.getInstance();
        //spawner.spawnShip(this);
        setPaintOrder(GameObject_TextFloat.class, Timer.class, GameObject_CustomTimer.class, ControlCentrum_UpgradeStars.class, ControlCentrum_Yard.class, ControlCentrum_Ship.class);
    }

    /**
     * calls the pause  menu whenever the escape key is pressed
     */
    public void act(){
        String key = Greenfoot.getKey();  
        if(key!=null && key.equals("escape"))  
        {     
            World nextWorld = new PauseMenu(this); // create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld); // create curtain object  
            int x = getWidth()/2;  
            int y = getHeight()/2;  
            addObject(fader, x, y); // add curtain into world    
            //Greenfoot.setWorld(new PauseMenu(this));
        }
        //if(!spawner.isSpawning())
        //spawner.spawnShip(this);
    }

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare(){  
        ControlCentrum_Yard yard = new ControlCentrum_Yard(Enum.YardType.YARD_REPAIR, Enum.Direction.EAST);
        addObject(yard, 790, 439);
        yard = new ControlCentrum_Yard(Enum.YardType.YARD_OIL, Enum.Direction.WEST);
        addObject(yard, 1100, 440);
        yard = new ControlCentrum_Yard(Enum.YardType.YARD_CONTAINER, Enum.Direction.WEST);
        addObject(yard, 1100, 620);   

        ControlCentrum_SpawnPoint spawn = new ControlCentrum_SpawnPoint(12000, 2000);
        addObject(spawn, 25, 245);   
        ControlCentrum_SpawnPoint spawn2 = new ControlCentrum_SpawnPoint(15000, 60000);
        addObject(spawn2, 25, 150);

        addObject(funding, 70, 17);              
        funding.increaseFunds(100000);
        addObject(score, 640, 17);

        GameObject_Clock clock = new GameObject_Clock("", new Color(0,0,0));
        addObject(clock, 1220, 17);

    }       

    /**
     * whenever a ship is clicked prepare for a click on the corrosponding yard/dock
     * adds/removes the selection rectangular to the ship
     */
    public void setShipClicked(ControlCentrum_Ship ship){
        this.ship = ship;
        if(selection != null)
        {
            removeObject(selection);
            selection = null;
        }
        if(ship != null)
        {
            if(selection == null)
            {
                selection = new ControlCentrum_SelectionBox(ship, ship.getImage().getWidth()+10, ship.getImage().getHeight()+10);
                addObject(selection, ship.getX(), ship.getY());
            }
        }
    }

    public ControlCentrum_Ship getShip(){
        if(this.ship != null){
            return this.ship;
        }

        else {
            return null;
        }
    }

    /**
     * if a ship leaves the screen without going through a yard/dock increases the shipsLetThrough variable
     * calls the GameOver function
     */
    public void updateShipsLetThrough(){
        shipsLetThrough++;
        gameOver();
    }
    
    /**
     * Calls the GameOver world whenever the shipsLetThrough variable exceeds 3
     */
    public void gameOver(){
        if(shipsLetThrough++ == 3){
            Greenfoot.setWorld(new GameOver("ControleCentrum", score.getScore()));
        }
    }

}
