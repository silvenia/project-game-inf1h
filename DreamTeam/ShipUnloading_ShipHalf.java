import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Write a description of class ExampleActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_ShipHalf  extends Actor
{
    private ShipUnloading world;
    
    private int containersWeight = 0;
    
    public ShipUnloading_ShipHalf()
    {
        setImage(new GreenfootImage(191, 250)); 
    }

    protected void addedToWorld(World world) {
        this.world = (ShipUnloading) world;          
    }
    
    
    public int calculateWeight(){
       int totalWeight = 0;
        List<ShipUnloading_Container> containersOnTop = getIntersectingObjects(ShipUnloading_Container.class);        
        for(int i = 0; i < containersOnTop.size(); i++)
        {
            if(containersOnTop.get(i).getCrane() == null)
                totalWeight += containersOnTop.get(i).getWeight();
        }
        
        containersWeight = totalWeight;        
        return containersWeight;
    }
    
    public int getWeight(){
        return containersWeight;
    }
}
