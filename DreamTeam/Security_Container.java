import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class SecurityContainer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Security_Container extends Actor
{
    
    public Security_Container(Enum.ContainerType containerType){
        switch(containerType){
            case RED:{
                setImage("Minigame 4/redContainerUp.png");
                break;
            }
            case RED_SIDE:{
                setImage("Minigame 4/redContainerSide.png");
                break;
            }
            case GREEN:{
                setImage("Minigame 4/greenContainerUp.png");
                break;
            }
            case GREEN_SIDE:{
                setImage("Minigame 4/greenContainerSide.png");
                break;
            }
        }
    }
    
    /**
     * Act - do whatever the SecurityContainer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
