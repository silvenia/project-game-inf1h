import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import java.lang.*;
import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
/**
 *  Burglar actor
 */
public class Security_Burglar extends SmoothMover
{
    private boolean searching = false;
    private boolean searched = false; // stores if a search has taken place.
    private Security_PathNode start;
    private Security_PathNode finish;
    private List<nodeSearch> unexplored = new LinkedList<nodeSearch>(); // open list
    private List<nodeSearch> explored = new LinkedList<nodeSearch>(); // closed list
    private double sqrt2 = Math.sqrt(2);
    private Security world;
    private List<nodeSearch> routePath;
    private List<nodeSearch> route = new LinkedList<nodeSearch>();
    private static final Color exploredColor = new Color(184, 150, 0);
    private static final Color routeColor = new Color(58, 184, 0);
    
    private static final int actsPerCycle = 15;
    private int currCycleCount = actsPerCycle;
    
    private boolean hasPath = false;
    private int movement = 0;
    
    private boolean escape;
    private boolean visible;
    private boolean steal = false;
    private boolean arrest = false;
    private Timer breakInTimer = null;
    private Timer loadInTimer = null;
    private GameObject_CustomTimer timer = null;
    private int escapePosition;
    private boolean removeBurglar = false;
    
    /**
     * constructor: retrieves paramater data
     */
    public Security_Burglar(Security_PathNode start, Security_PathNode finish) {
        setImage("images/Minigame 4/invisible.png");
        this.start = start;
        this.finish = finish;
        this.escape = false;
        this.visible = false;
    }
    
    public void addedToWorld(World world) {
        this.world = (Security) world;             
    }
    
    /**
     * tells the burglar what to do:
     * - go to container through the given path
     * - break in to the container
     * - try to runaway
     * - remove this burglar
     */
    public void act() 
    {             
        if(removeBurglar)
        {
            getWorld().removeObject(this);
            return;
        }
            
        if(isVisible())
        {
            if(!isTouching(Security_LightRadius.class))
                setVisibility(false);
            else
            setImage("images/Minigame 4/burglar_right.png");
        }
        else
            setImage("images/Minigame 4/invisible.png");                   
        
        if(!isStealing() && !isBeingArrested())
        {
            if(hasPath && !searching){
                if(movement == 0){
                    turnTowards(finish.getX(), finish.getY());  
                    move(1);
                    if((getX() == finish.getX() && getY() == finish.getY()))
                    {
                        if(!isEscaping())
                        {
                            movement = 0;
                            hasPath = false;
                            unexplored = new LinkedList<nodeSearch>();
                            explored = new LinkedList<nodeSearch>();
                            route = new LinkedList<nodeSearch>();
                            steal();        
                            escapePosition = Greenfoot.getRandomNumber(8);
                            this.start = world.getStartingPoint().get(escapePosition);
                            startSearch(this.finish, this.start);
                            //startSearch(this.finish, this.start);
                        }
                        else
                            escape();
                            
                    }                   
                }
                else{           
                    turnTowards(route.get(movement).node.getXcoord(), route.get(movement).node.getYcoord());
                    move(1);
                    if(getX() == route.get(movement).node.getXcoord() && getY() == route.get(movement).node.getYcoord())
                    {
                        movement--;
                    }                  
                }
            }
            else{
                if(searching) {
                    if(currCycleCount >= actsPerCycle) {
                        currCycleCount = 0;
                        findPath();
                    }
                    currCycleCount++;
                } else if(!searched && !hasPath) {        
                    startSearch(this.start,  this.finish);
                }
            }
        }
    }
    
    /**
     * intiates pathfinding
     */
    public void startSearch(Security_PathNode start, Security_PathNode finish) {
        this.start = start;
        this.finish = finish;
        searching = true;
        hasPath = true;
        unexplored.add(
            new nodeSearch(start, 0, Double.MAX_VALUE, null));        
    }
    
    /**
     * this and expandNodes do the grunt of the actual pathfinding.
     */
    private void findPath() {
        if(unexplored.size()==0) {
            //if there are no more nodes to explre then we couldn't find a path.
            finishedPath(null);
        } else {
            ListIterator<nodeSearch> explorer = unexplored.listIterator();
            int nodePoz = 0;
            double lowestScore = Double.POSITIVE_INFINITY;
            // to start with the best score is inf because it needs to be higher than
            // any possible node in the list.
            nodeSearch bestNode = null;
            // go through each unexplored node searching for the lowest.
            while(explorer.hasNext()) {
                int nextNodePoz = explorer.nextIndex();
                nodeSearch node = explorer.next();
                if(node.getFVal() < lowestScore) {
                    lowestScore = node.getFVal();
                    bestNode = node;
                    nodePoz = nextNodePoz;
                }
            }
            // remove it from the unexplored list and add it to the explored list.
            unexplored.remove(nodePoz);
            explored.add(bestNode);
            // search all it's neighbours
            List<traversalCost> nodesFound = bestNode.getNode().getPathEdges();
            //bestNode.getNode().changeColor(exploredColor);
            expandNodes(bestNode, nodesFound);
        }
    }
    
    private void expandNodes(nodeSearch parent, List<traversalCost> nodesFound) {
        for(traversalCost newNode : nodesFound) {
            if(parent.getParent() != null) {
                if(parent.getParent().getNode() == newNode.getNode()) {
                    continue; // returning to parent node is pointless!
                }
            }
            /*if(explored.indexOf(newNode) > -1) {
                continue;
            }*/
            double cost = parent.getCost() + newNode.getCost();
            boolean cheaperRouteExists = false;
            ListIterator<nodeSearch> explorer = unexplored.listIterator();
            // go through each node we have already found to see if this
            // is the same node
            while(explorer.hasNext()) {
                int nextNodePoz = explorer.nextIndex();
                nodeSearch node = explorer.next();
                // if we find a shorter route to the same node.
                if(node.getNode() == newNode.getNode()) {
                    if(cost < node.getCost()) {
                        // if our route to the node is faster then remove the other node
                        cheaperRouteExists = false;
                        unexplored.remove(nextNodePoz);
                    } else { // keep the other node
                        cheaperRouteExists = true;
                    }
                    break;
                }
            }
            ListIterator<nodeSearch> explorer2 = explored.listIterator();
            while(explorer2.hasNext()) {
                int nextNodePoz = explorer2.nextIndex();
                nodeSearch node = explorer2.next();
                if(node.getNode() == newNode.getNode()) {
                    if(cost < node.getCost()) {
                        cheaperRouteExists = false;
                        explored.remove(nextNodePoz);
                    } else {
                        cheaperRouteExists = true;
                    }
                    break;
                }
            }
            if(cheaperRouteExists) {
                //world.canvas.drawLine(parent.getNode(), newNode.getNode(), new Color(191, 0, 0));
                //world.canvas.drawArrows(parent.getNode(), newNode.getNode(), new Color(191, 0, 0));
                continue;
            }
            // calculate the heuristic:
            double h = heuristicEuclidean(newNode.getNode(), finish);
            // calculate the F score
            double FVal = cost + h;
            // create a new nodeSearch object for this newly explored node.
            nodeSearch newNodeSearch = new nodeSearch(newNode.getNode(), cost, FVal, parent);
            //world.canvas.drawLine(parent.getNode(), newNode.getNode(), exploredColor);
            //world.canvas.drawArrows(parent.getNode(), newNode.getNode(), exploredColor);
            if(newNode.getNode() == finish) {
                // see if it is the finish node, if so we are done.
                finishedPath(newNodeSearch);
                //System.out.println("finished: " + cost);
                searching = false;
            } else {
                unexplored.add(newNodeSearch);
            }
        }
    }
    
    /**
     * straight line tiebraker. Prefers nodes that are closest to direct line (as the crow flys)
     * between the start and the finish. (can have strange results with 4 directional movement and
     * obsticles, but still fastest path.)
     */
    private double tiebreaker(Security_PathNode a, Security_PathNode b) {
        double dx1 = a.getX() - b.getX();
        double dy1 = a.getY() - b.getY();
        double dx2 = start.getX() - b.getX();
        double dy2 = start.getY() - b.getY();
        return Math.abs(dx1*dy2 - dx2*dy1)*0.001;
        // I don't expect the path to ever be more than 1000 long so this will
        // never be big enough to bias the pathfinder to a non-optimal route.
    }
    
    /**
     * euclidean heuristic - estimates straight line distance from start to finish
     * good for any directional movement.
     */
    private double heuristicEuclidean(Security_PathNode a, Security_PathNode b) {
        return Math.sqrt( (b.getXcoord() - a.getXcoord())*(b.getXcoord() - a.getXcoord())
            + (b.getYcoord() - a.getYcoord())*(b.getYcoord() - a.getYcoord()));
    }
    
    /**
     * what to do when we have found a path........
     */
    private void finishedPath(nodeSearch endNode) {
        if(endNode == null) {
            System.out.println("No path found");
            searching = false;
            searched = true;
        } else {
            
            while(endNode.getParent() != null) {
                //world.canvas.drawLine(endNode.getParent().getNode(), endNode.getNode(), routeColor);
                //world.canvas.drawArrows(endNode.getParent().getNode(), endNode.getNode(), routeColor);
                route.add(endNode);
                movement = route.size() - 1;
                endNode = endNode.getParent();
                //endNode.getNode().changeColor(routeColor);                
                //Greenfoot.stop();
            }
        }
    }
    
    public void setVisibility(boolean visibility){
        this.visible = visibility;
    }
    public boolean isVisible(){
        return this.visible;
    }
    public boolean isEscaping(){
        return this.escape;   
    }
    public void escape(){
        if(isEscaping())
        {
            if(escapePosition == 6 || escapePosition == 7 && loadInTimer == null){
                if(world.getBoat() != null){
                    if(world.getBoat().getX() >= 550 && world.getBoat().getX() <= 760)
                    {
                        escapeWithBoat();                                               
                    }
                }
            }
            else{
                world.reduceSatisfaction(15);
                world.removeObject(this); 
            }
        }
    }
    
    public boolean isStealing(){
        return this.steal;
    }
    private void steal() {
        if(breakInTimer != null)
            return;
        if(timer == null)
            timer = new GameObject_CustomTimer(null);
        getWorld().addObject(timer, getX(), getY() - 50);   
        
        steal = true;
        timer.start(5.0d);
        
        breakInTimer = new Timer();
        breakInTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timer.substractTime(0.1d);
                final double timePercent = 5.0d;
                if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                {
                    timer.setImage("Timer-4.png");
                                       }
                else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                {
                    timer.setImage("Timer-3.png");
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                {
                    timer.setImage("Timer-2.png");
                }
                
                if(timer.getTimeLeft() <= 0 || isBeingArrested())
                {                                        
                    timer.removeTimer();
                    timer = null;                    
                    escape = true;
                    steal = false;     
                    breakInTimer.cancel();   
                    breakInTimer = null;
                }       
            }
        }, 100, 100);  
        
        
    }    
    
    public boolean isBeingArrested(){
        return arrest;        
    }
    public void arrest(boolean arrest){
        this.arrest = arrest;
    }
    
    public void escapeWithBoat(){
        if (loadInTimer != null)
            return;
        
        if(timer == null)
            timer = new GameObject_CustomTimer(null);
            
        getWorld().addObject(timer, getX(), getY() - 50);    
        timer.start(2.5d);
        boolean canRemove = false;
        loadInTimer = new Timer();
        
        loadInTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timer.substractTime(0.1d);
                final double timePercent = 2.5d;
                if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                {
                    timer.setImage("Timer-4.png");
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                {
                    timer.setImage("Timer-3.png");
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                {
                    timer.setImage("Timer-2.png");
                }
                
                if(timer.getTimeLeft() <= 0)
                {                          
                    timer.removeTimer(); 
                    timer = null;
                    if(world.getBoat()!= null)
                    {
                        if(world.getBoat().getX() >= 550 && world.getBoat().getX() <= 760)
                        {
                            if(world.getBoat().loadBurglar())
                                removeBurglar = true;                                  
                        }
                    }     
                    loadInTimer.cancel();
                    loadInTimer = null;
                }     
            }
        }, 100, 100);          
    }
}