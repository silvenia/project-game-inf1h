import greenfoot.*;  

/**
 * This class handles the sound effects.
 * 
 * @author (INF1H-#1) 
 * @version (a version number or a date)
 */
public class SoundEffect  
{
    GreenfootSound gameEffect;   

    public enum Effect{
        CONSTRUCTION,
        REPAIR,
        UNLOADING,
        ARREST,
        SAILING_BOAT,
        POP
    }

    /**
     * This method plays an effect.
     * 
     * @param  effect   Grabs the effect based on the enum given.
     * @param  playOnce   Tell the effect to play once or loop it.
     */   
    public void playEffect(Effect effect, boolean playOnce)
    {
        switch(effect)
        {
            case CONSTRUCTION:
            {
                gameEffect = new GreenfootSound("Effects/Construction.mp3");
                break;
            }
            case REPAIR:
            {
                gameEffect = new GreenfootSound("Effects/Repair.mp3");
                break;
            }
            case UNLOADING:
            {
                gameEffect = new GreenfootSound("Effects/Unloading.mp3");
                break;
            }       
            case ARREST:
            {
                gameEffect = new GreenfootSound("Effects/Arrest.mp3");
                break;
            }
            case SAILING_BOAT:
            {
                gameEffect = new GreenfootSound("Effects/Sailing Boat.mp3");
                break;
            }   
            case POP:
            {
                gameEffect = new GreenfootSound("Effects/Pop.mp3");
                break;
            }                   
        }
        gameEffect.setVolume(50);
        if(gameEffect != null)
        {
            if(playOnce)
                gameEffect.play();   
            else
                gameEffect.playLoop();
        }
    }
    
    /**
     * This method stops an effect if it is playing. Used in situations where effects are played as loops.
     * 
     */       
    public void stopEffect()
    {
        if(gameEffect != null)
        {
            gameEffect.stop();
        }
    }
}
