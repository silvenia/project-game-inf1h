import greenfoot.*;  
  
/**
 * creates a fader out/in effect
 */
public class MenuObject_Fader extends Actor  
{  
    private int transVal;  
    private int direction;  
    World nextWorld;  
  
    public MenuObject_Fader(World inWorld)  
    {  
        nextWorld = inWorld;  
        if (nextWorld == null) direction = -4;  else direction = 4;  
    }  
  
    /**
     * event that triggers if the actor is added to the world
     */
    public void addedToWorld(World world)  
    {  
        setImage(new GreenfootImage(world.getWidth(), world.getHeight()));  
        getImage().fill(); // black is default color for new GreenfootImages  
    }  
  
    /**
     * actual code for the fade effect
     */
    public void act()  
    {  
        transVal = (transVal+direction+256)%256; // update value  
        if (transVal == 0) // check value to see if we are done yet  
        {  
            if (nextWorld == null) getWorld().removeObject(this); // for new world  
            else {
                Greenfoot.setWorld(nextWorld); // for old world  
                getWorld().removeObject(this);
            }
        }  
        else getImage().setTransparency(transVal); // for when not done yet  
    }  
}  