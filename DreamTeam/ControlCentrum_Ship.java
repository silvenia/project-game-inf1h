import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.awt.Color;

/**
 * Ship object
 * 
 */
public class ControlCentrum_Ship extends SmoothMover 
{    
    private double unloadTime;
    private double repairTime;  
    private Enum.ShipType shipType;
    private Enum.ShipState shipState;
    private boolean shipClicked = false;    
    private float speed;
    private boolean isDocked = false;
    public int destination = 0;
    private Timer dockTimer;
    private Timer collisionTimer;
    private boolean hasCollision = false;
    private int points;
    private int funds;  
    private GreenfootImage emptyShip;
    private GreenfootImage ship;
    private Coord coord;
    ControlCentrum centrum;
   
    /**
     * constructor: retrieves parameter data and calls the checkType function
     */
    public ControlCentrum_Ship(final Enum.ShipType shipType, final Enum.ShipState shipState){        
        this.shipType = shipType;
        this.shipState = shipState;
        
        checkType();
    }

    protected void addedToWorld(World world)
    {
        this.centrum = (ControlCentrum)world;
    }
    
    /**
     * checks for collision, makes the ship that causes the collision stop
     */
    public void act(){        
        checkClick();
        List<ControlCentrum_Ship> ships = getIntersectingObjects(ControlCentrum_Ship.class);
        if(!hasCollision)
        {
            if(ships.size() > 0)
            {
                for(ControlCentrum_Ship ship : ships)  
                {  
                    if(ship.getX() > getX() + 20 || ship.getY() > getY() + 5)
                        hasCollision = true;
                        damageShip();
                        break;
                } 
            }
        }
        
        if(!hasCollision){
            if(coord != null){
                sailToLocation();
            }
            else
                move(speed);
                        
            goAway();
        }    
    }
        
    /**
     * Method which checks the type of the incoming ship and will set its image
     * and also also assign the variables for unload and repair time accordingly
     */
    public void checkType(){            
        switch(shipType)
        {
            case OIL_SMALL:
            {
                unloadTime = 3.0d;
                setSpeed(0.6f);
                if(shipState == Enum.ShipState.DAMAGED){
                    setImage(new GreenfootImage("images/Minigame 1/Ship_Oil_Small-damaged.png"));
                    this.repairTime = 3.0d;
                    this.points = 25;
                    this.funds = 250;
                }
                else
                {                    
                
                    this.points = 50;
                    this.funds = 500;
                }
                    this.ship = new GreenfootImage("images/Minigame 1/Ship_Oil_Small.png");
                    this.emptyShip = new GreenfootImage("images/Minigame 1/Ship_Oil_Small-empty.png");
                break;
            }
            case OIL_MEDIUM:
            {
                unloadTime = 4.0d;
                setSpeed(0.55f);
                if(shipState == Enum.ShipState.DAMAGED){
                    setImage(new GreenfootImage("images/Minigame 1/Ship_Oil_Medium-damaged.png"));
                    repairTime = 3.5d;
                    this.points = 40;
                    this.funds = 400;
                }
                else
                {                          
                    this.points = 80;
                    this.funds = 800;
                }
                    this.ship = new GreenfootImage("images/Minigame 1/Ship_Oil_Medium.png");  
                    this.emptyShip = new GreenfootImage("images/Minigame 1/Ship_Oil_Medium-empty.png");
                break;
            }
            case OIL_LARGE:
            {
                unloadTime = 5.0d;
                setSpeed(0.5f);
                if(shipState == Enum.ShipState.DAMAGED){
                    setImage(new GreenfootImage("images/Minigame 1/Ship_Oil_Large-damaged.png"));
                    repairTime = 4.0d;
                    this.points = 50;
                    this.funds = 500;
                }
                else
                {                    
                    this.points = 100;
                    this.funds = 1000;   
                }
                    this.ship = new GreenfootImage("images/Minigame 1/Ship_Oil_Large.png");   
                    this.emptyShip = new GreenfootImage("images/Minigame 1/Ship_Oil_Large-empty.png");
                break;
            }
            case CONTAINER_SMALL:
            {
                unloadTime = 3.0d;
                setSpeed(0.6f);
                if(shipState == Enum.ShipState.DAMAGED){
                    setImage(new GreenfootImage("images/Minigame 1/Ship_Container_Small-damaged.png"));
                    repairTime = 3.0d;
                    this.points = 25;
                    this.funds = 250;
                }
                else                    
                {
                    this.points = 500;
                    this.funds = 500;    
                }
                this.ship = new GreenfootImage("images/Minigame 1/Ship_Container_Small.png");
                    this.emptyShip = new GreenfootImage("images/Minigame 1/Ship_Container_Small-empty.png");
                break;
            }
            case CONTAINER_MEDIUM:
            {
                unloadTime = 4.0d;
                setSpeed(0.55f);
                if(shipState == Enum.ShipState.DAMAGED){
                    setImage(new GreenfootImage("images/Minigame 1/Ship_Container_Medium-damaged.png"));
                    repairTime = 3.5d;
                    this.points = 40;
                    this.funds = 400;
                }
                else
                {                                        
                    this.points = 80;
                    this.funds = 800;    
                }
                    this.ship = new GreenfootImage("images/Minigame 1/Ship_Container_Medium.png");
                    this.emptyShip = new GreenfootImage("images/Minigame 1/Ship_Container_Medium-empty.png");
                break;
            }
            case CONTAINER_LARGE:
            {
                unloadTime = 5.0d;
                setSpeed(0.5f);
                if(shipState == Enum.ShipState.DAMAGED){
                    setImage(new GreenfootImage("images/Minigame 1/Ship_Container_Large-damaged.png"));
                    repairTime = 4.0d;
                    this.points = 50;
                    this.funds = 500;
                }
                else
                {                     
                    
                    this.points = 100;
                    this.funds = 1000;   
                }
                    this.ship = new GreenfootImage("images/Minigame 1/Ship_Container_Large.png");
                    this.emptyShip = new GreenfootImage("images/Minigame 1/Ship_Container_Large-empty.png");
                break;
            }
        }
        
        if(getShipState() != Enum.ShipState.DAMAGED)
            setImage(this.ship);
        if(getShipState() == Enum.ShipState.DAMAGED)
            setSpeed(0.45f);
    }
            
    /**
     * Method which checks if a ship has been clicked
     */
    public void checkClick(){
        if(Greenfoot.mouseClicked(this)){            
            shipClicked = true;
            ControlCentrum c = (ControlCentrum) getWorld();
            if(c.getShip() == this)
                c.setShipClicked(null);
            else
                c.setShipClicked(this);
            //c.addObject (new SelectionBox(this, getImage().getWidth()+10, getImage().getHeight()+10), getX(), getY());
            /*if(l.isEmpty()) {
                c.addObject (new shipClicked(this), getX(), getY() -40);
            }
            else {
                for (int i = 0; i < l.size(); i ++){
                    shipClicked sc = (shipClicked) l.get(i);
                    GreenfootImage gi = (GreenfootImage) sc.getImage();
                    gi.setTransparency(0);
                    c.addObject(new shipClicked(this), getX(), getY()-40);
        }
        
    }*/
}
}
    
    /**
     * Method which controls the automated system of sailing away after service is provided
     */
    public void goAway(){                    
        if(isAtEdge()){
            ControlCentrum c = (ControlCentrum) getWorld();
            if(unloadTime == 0 && repairTime == 0){
                getWorld().removeObject(this);
            }
            
            else {
                
                if(getY() < 250)
                    c.updateShipsLetThrough();                    
                getWorld().removeObject(this);
               
            }
            if(c.getShip() == this)
                c.setShipClicked(null);
        }
    }

    public final void setSpeed(final float speed){
        this.speed = speed;
    }
    
    public final double getRepairTime(){
        return this.repairTime;
    }
    
    public final Enum.ShipType getShipType(){
        return this.shipType;
    }
    
    public final Enum.ShipState getShipState(){
        return this.shipState;
    }
    
    public final void setShipState(final Enum.ShipState shipState)
    {
        this.shipState = shipState; 
    }

    public void setCoordinates(final Coord coord)
    {
        this.coord = coord;
    }
    public void updateUnloadTime(){
        //if(isDocked && getUnloadTime() > 0){
            //unloadTime--;
        //} 
    }
    
    public void updateRepairTime(){
        if(isDocked && getRepairTime() > 0){
            repairTime--;
        }
    }
    
    public void damageShip()
    {
        if(!hasCollision)
            return;
            
        final GameObject_CustomTimer timer = new GameObject_CustomTimer(null);
        getWorld().addObject(timer, getX(), getY());
            
        timer.start(3.0d);
                    
        ActionListener taskPerformer = new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) {
                timer.substractTime(0.1d);
                final double timePercent = 3.0d;
                if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                {
                    timer.setImage("Timer-4.png");
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                {
                    timer.setImage("Timer-3.png");
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                {
                    timer.setImage("Timer-2.png");
                }
                
                    if(timer.getTimeLeft() <= 0)
                    {                    
                        collisionTimer.stop();
                        timer.removeTimer();
                        hasCollision = false;
                        setImage(ship);
                    }    
               }
        };
        
        collisionTimer = new Timer(100, taskPerformer);   
        collisionTimer.setInitialDelay(200);
        collisionTimer.start();        
    }
        
    public void dockShip()
    {
        if(dockTimer != null || isDocked == false)
            return;
           
        final ControlCentrum c = (ControlCentrum)getWorld();
        final GameObject_CustomTimer timer = new GameObject_CustomTimer(null);                
        getWorld().addObject(timer, getX(), getY()); 
        
        ControlCentrum_Yard yard = (ControlCentrum_Yard)getOneIntersectingObject(ControlCentrum_Yard.class);
        final SoundEffect sound = new SoundEffect(); 
        double loadTime = 0;
        if(getShipState() != Enum.ShipState.DAMAGED)
        {
            loadTime = yard.getLoadingTime(unloadTime);
            sound.playEffect(SoundEffect.Effect.UNLOADING, false);  
        }
        else
        {
            loadTime = yard.getLoadingTime(repairTime);
            sound.playEffect(SoundEffect.Effect.REPAIR, false);  
        }
            
        timer.start(loadTime);
        ActionListener taskPerformer = new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) {
                timer.substractTime(0.1d);
                final double timePercent = unloadTime;
                if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                {
                    timer.setImage("Timer-4.png");
                    if(getShipState() != Enum.ShipState.DAMAGED)
                        unloadShip();
                    else
                        repairShip();
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                {
                    timer.setImage("Timer-3.png");
                }
                else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                {
                    timer.setImage("Timer-2.png");
                }
                
                if(timer.getTimeLeft() <= 0)
                {                    
                    dockTimer.stop();
                    timer.removeTimer();
                    isDocked = false;
                    setCoordinates(null);
                    sound.stopEffect();
                    c.score.addScore(points);
                    c.funding.increaseFunds(funds);
                    GameObject_TextFloat textFloat = new GameObject_TextFloat(Integer.toString(points) + " points", new Color(251,175,93));
                    getWorld().addObject(textFloat, getX(), getY() - 40);
                    textFloat = new GameObject_TextFloat(Integer.toString(funds) + " funds", new Color(251,175,93));
                    getWorld().addObject(textFloat, getX(), getY() - 10);
                    sound.stopEffect();
                }                
            }
        };
        
        dockTimer = new Timer(100, taskPerformer);   
        dockTimer.setInitialDelay(200);
        dockTimer.start();
    }
    
    public void unloadShip()
    {
        setImage(emptyShip);
    }
    
    public void repairShip()
    {
        setImage(ship);
    }
    public void sailToLocation(){        
        int xLoc = getX();
        int yLoc = getY();
        int rotatePointX = 0;
        int hangarY = 0;
        int hangarX = 0;
                
        if(getX() >= coord.x && (getX() < coord.x + 50))
        {
            if(getRotation() == 90){
                if(getY() >= coord.y)
                {
                    move(0);
                    isDocked = true;
                    dockShip();
                    //setCoordinates(null);
                }
                else
                    move(speed);
            }
            else
            {
                turn(1);
                move(0);
            }
        }
        else
            move(speed);
        /*switch(destination){
            //set coordinates for different Warehouses
            case 1: 
            {
                rotatePointX = 820;
                hangarY = 450;
                hangarX = 829;
                break;
            }
            case 2: 
            {
                rotatePointX = 1040;
                hangarY = 450;
                hangarX = 1049;
                break;
            }
            case 3: 
            {
                rotatePointX = 1040;
                hangarY = 620;
                hangarX = 1049;
                break;
            }
            default: break;            
        }
        if(xLoc <= rotatePointX)
         move(speed);
        //rotate at the set rotatepoint as set above
        if(xLoc > rotatePointX){
            if(getRotation() == 90){
                //if turned 90 degrees, move forward again
                //move(speed);
                if(xLoc > hangarX && xLoc < 1080){
                     setLocation(xLoc - 1, yLoc);
                    }
                if(yLoc > hangarY){
                    move(0);
                    isDocked = true;
                    dockShip();
                }
                else{
                    //move forward if not at the warehouse yet
                    move(speed);
                    //move sideways to the warehouse

                }
            }
            else{
                //only rotate if sailing on a river
                if((xLoc > 300 && xLoc < 600) || (xLoc > 780 && xLoc < 1050)){
                turn(1);
                move(0);
                    
                }
            }
        }      
        //move(0);*/
    }
    
    public final boolean abc(){
        return shipClicked;
    }
}
