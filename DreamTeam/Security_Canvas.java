import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
import java.lang.Math.*;

/**
 * debugs the pathfinding
 */
public class Security_Canvas extends Actor
{
    public static final int arrowOffset = 12; //pixels
    public static final int arrowLength = 8; //pixels
    public static final int arrowAngle = 35; //degrees
    public static final int textOffset = 12; //pixels
    private GreenfootImage img;
    
    public Security_Canvas(int width, int height) {
        img = new GreenfootImage(width, height);
        setImage(img);
    }
    
    /**
     * sets color and draws line from x1,y1 till x2,y2
     */
    public void drawLine(int x1, int y1, int x2, int y2, Color colour) {
        img.setColor(colour);
        img.drawLine(x1, y1, x2, y2);
    }
    
    /**
     * draw line from x1,y1 till x2,y2 with given color
     */
    public void drawLine(Security_PathNode pN1, Security_PathNode pN2, Color colour) {
        int x1 = pN1.getXcoord();
        int y1 = pN1.getYcoord();
        int x2 = pN2.getXcoord();
        int y2 = pN2.getYcoord();
        drawLine(x1, y1, x2, y2, colour);
    }
    
    /**
     * Ridiculously complicated function just to draw an arrow in the direction
     * the line is pointing.
     */
    public void drawArrows(int x1, int y1, int x2, int y2, Color colour) {
        double midPointX = (x1 + x2)/2;
        double midPointY = (y1 + y2)/2;
        double dy = y2-y1;
        double dx = x2-x1;
        double grad = dy/dx; // for some obscure reason (y2-y1)/(x2-x1) didn't work
        // angle of line counter clockwise from the east (x-axis)
        double angle = Math.atan(grad);
        // the x and y coordinates of the point 10 pixels along the line relative
        // to the midpoint can be found with trig using the angle we just found
        // hyp as 10 pixels, opp as y difference and adj as x diff.
        double xOffset = arrowOffset*Math.cos(angle); // Adj = hyp * cos(theta)
        double yOffset = arrowOffset*Math.sin(angle); // Opp = hyp * sin(theta)
        double offsetX;
        double offsetY;
        img.setColor(colour);
        if(x1>x2) { // support for uni-directional waypoints.
            offsetX = midPointX - xOffset;
            offsetY = midPointY - yOffset;
            double line1Angle = angle - Math.toRadians(arrowAngle);
            double line1x = arrowLength*Math.cos(line1Angle);
            double line1y = arrowLength*Math.sin(line1Angle);
            double line2Angle = angle + Math.toRadians(arrowAngle);
            double line2x = arrowLength*Math.cos(line2Angle);
            double line2y = arrowLength*Math.sin(line2Angle);
            img.drawLine((int)offsetX, (int)offsetY,
                (int)line1x+(int)offsetX, (int)line1y+(int)offsetY);
            img.drawLine((int)offsetX, (int)offsetY,
                (int)line2x+(int)offsetX, (int)line2y+(int)offsetY);
        } else {
            offsetX = midPointX + xOffset;
            offsetY = midPointY + yOffset;
            double line1Angle = angle - Math.toRadians(180-arrowAngle);
            double line1x = arrowLength*Math.cos(line1Angle);
            double line1y = arrowLength*Math.sin(line1Angle);
            double line2Angle = angle + Math.toRadians(180-arrowAngle);
            double line2x = arrowLength*Math.cos(line2Angle);
            double line2y = arrowLength*Math.sin(line2Angle);
            img.drawLine((int)offsetX, (int)offsetY,
                (int)line1x+(int)offsetX, (int)line1y+(int)offsetY);
            img.drawLine((int)offsetX, (int)offsetY,
                (int)line2x+(int)offsetX, (int)line2y+(int)offsetY);
        }
    }
    
    public void drawArrows(Security_PathNode pN1, Security_PathNode pN2, Color colour) {
        int x1 = pN1.getXcoord();
        int y1 = pN1.getYcoord();
        int x2 = pN2.getXcoord();
        int y2 = pN2.getYcoord();
        drawArrows(x1, y1, x2, y2, colour);
    }
    
    public void drawText(int x1, int y1, int x2, int y2, Color colour) {
        double midPointX = (x1 + x2)/2;
        double midPointY = (y1 + y2)/2;
        double dy = y2-y1;
        double dx = x2-x1;
        double grad = dy/dx; // for some obscure reason (y2-y1)/(x2-x1) didn't work
        img.setColor(colour);
        double dist = Math.sqrt( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2) );
        String distance = String.format("%5.1f", dist);
        double perpendicularGrad = (grad==0)?java.lang.Double.POSITIVE_INFINITY:-1/grad;
        double perpendicularAngle = Math.atan(perpendicularGrad);
        double textOffsetX = textOffset*Math.cos(perpendicularAngle);
        double textOffsetY = textOffset*Math.sin(perpendicularAngle);
        img.drawString(distance, (int)midPointX+(int)textOffsetX,
                                       (int)midPointY+(int)textOffsetY);
        //
    }
    
    /**
     * draws text
     */
    public void drawText(Security_PathNode pN1, Security_PathNode pN2, Color colour) {
        int x1 = pN1.getXcoord();
        int y1 = pN1.getYcoord();
        int x2 = pN2.getXcoord();
        int y2 = pN2.getYcoord();
        drawText(x1, y1, x2, y2, colour);
    }
}