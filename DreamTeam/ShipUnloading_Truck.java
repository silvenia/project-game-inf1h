import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Write a description of class ExampleActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_Truck extends ShipUnloading_Transport
{    
    public ShipUnloading_Truck(ShipUnloading_TrafficLight trafficLight)
    {
        super(trafficLight);
        setImage("Minigame 2/Truck.png");
        turn(270);
        this.speed = 2;
        this.WAIT_POINT = 300;
    }       
    
    public void setLoaded(boolean loaded){
        this.loaded = loaded;
        if(loaded)
            setImage("Minigame 2/Truck_loaded.png");
    }    
    
    public void spawnTransport(){
        int location = 0;
        
        if(getX() > 700)
            location = ShipUnloading.LOCATION_AI;
        else
            location = ShipUnloading.LOCATION_PLAYER;
        world.spawnTransport(this, ShipUnloading.TRANSPORT_TRUCK, location);
    }
}
