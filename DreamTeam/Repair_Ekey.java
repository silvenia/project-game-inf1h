import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * 'E' Key object, used to show the player to pick a certain item up with the E key
 * 
 */
public class Repair_Ekey extends Actor
{
    /**
     * sets the image
     */
    public Repair_Ekey(){
        setImage("Minigame 3/Ekey.png");
    }   
}
