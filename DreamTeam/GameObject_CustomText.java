import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.Color;  

/**
 * Write a description of class Scores here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameObject_CustomText extends GameObject_Hud
{
    private String text;
    private Color color;
    /**
     * Act - do whatever the Scores wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */    
    
    public GameObject_CustomText(String text, Color color)
    {        
        super(text.length() * 12, 20, color);
        this.text = text;       
        this.color = color;
    }

    protected void addedToWorld(World world)
    {
        updateText();
    }
    public void act()
    {                
    }    
    
    private void updateText()
    {
        GreenfootImage image = new GreenfootImage(text, 24, color, null);        
        //image.drawString(text, 1, 16); 
        setImage(image);
    }
    
    public void setText(String text){
        this.text = text;
        updateText();
    }
}
