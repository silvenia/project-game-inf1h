import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Creates the main menu
 */
public class MainMenu extends World
{

    /**
     * sets background and background music.
     * Also starts the game automaticly
     * 
     */
    public MainMenu()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Menu/MenuBackground.png");
        //backgroundMusic.playLoop();  
        //MenuLayout menu = new MenuLayout();
        //menu.setImage("Menu/MenuLayout.png");
        //addObject(menu,644,433);
        Sound music = Sound.getInstance();
        music.setMusic(Sound.MusicTrack.MENU);
        prepare();
        addObject(new MenuObject_Fader(null), getWidth()/2, getHeight()/2); 
        Greenfoot.start();
    }   

    /**
     * Called from constructor: places Play, Highscore, Options, Credits and Exit button on the screen.
     */
    private void prepare()
    {
        int centerX = getWidth() / 2;
        int centerY = getHeight() / 2;

        // Create buttons
        PlayButton play = new PlayButton(new GameMenu(this));
        play.setImage("Menu/MainMenu/PlayButton.png");

        HighscoreButton highscore = new HighscoreButton();
        highscore.setImage("Menu/MainMenu/HighscoreButton.png");

        OptionsButton options = new OptionsButton();
        options.setImage("Menu/MainMenu/OptionsButton.png");

        CreditsButton credits = new CreditsButton();
        credits.setImage("Menu/MainMenu/CreditsButton.png");

        ExitButton exit = new ExitButton();
        exit.setImage("Menu/MainMenu/ExitButton.png");

        // Add buttons and position them into the world ( MainMenu )
        addObject(play, centerX, centerY + 15);
        addObject(highscore, centerX, centerY + 75);
        addObject(options, centerX, centerY + 135);
        addObject(credits, centerX, centerY + 195);
        addObject(exit, centerX, centerY + 255);

    }
}
