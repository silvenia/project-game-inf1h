import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.geom.Rectangle2D;  
import java.awt.font.FontRenderContext;  

/**
 * creates the option menu, the option menu is a sub menu of the main menu.
 * contains the option for the player to adjust the volume.
 */
public class OptionsMenu extends World  
{

    /**
     * Constructor for objects of class OptionsMenu.
     * 
     */
    public World world; 
    
    public OptionsMenu(World world)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Menu/MenuBackground.png");
        this.world = world;
        prepare();
        addObject(new MenuObject_Fader(null), getWidth()/2, getHeight()/2); 
    }
    
    private void prepare()
    {
        int centerX = getWidth() / 2;
        int centerY = getHeight() / 2;
        
        Checkbox box = new Checkbox();
        //box.setImage("UI/Box_Checked.png");
        
        BackButton back = new BackButton(world);
        back.setImage(new GreenfootImage("Menu/BackButton.png"));
        
        GameObject_Slider slider = GameObject_Slider.getInstance();        
        
        addObject(box, centerX -145, centerY + 20);  
        addObject(slider, centerX + 60, centerY + 30);
        addObject(back, centerX, centerY + 255);
        
        Font font = new Font("Cooper Black", Font.BOLD, 26);  
        GreenfootImage g = getBackground();  
        g.setFont(font);  
        String condString = "Music";  
        FontRenderContext frc = ((Graphics2D)g.getAwtImage().createGraphics()).getFontRenderContext();    
        Rectangle2D boundsCond = font.getStringBounds(condString, frc);  
        int condWidth = (int)boundsCond.getWidth();  
        int strHeight = (int)boundsCond.getHeight(); 
        
        Color rgbColor = new Color(0,0,0);
        g.setColor(rgbColor);  
        
        g.drawString(condString, centerX - 130, centerY + 30);  
        
    }
}
