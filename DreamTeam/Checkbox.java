import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Checkbox here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Checkbox extends MenuActors
{
    private boolean isChecked;
    Sound music = Sound.getInstance();
    
    /**
     * Act - do whatever the Checkbox wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public Checkbox()
    {
        if(music.isMusicPlaying())
        {
            setImage("UI/Box_Checked.png");
            isChecked = true;
        }
        else
        {
            setImage("UI/Box_Unchecked.png");
            isChecked = false;
        }
    }
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
            if(!isChecked)
            {
                setImage("UI/Box_Checked.png");
                isChecked = true;
                music.startMusic();
            }
            else
            {
                setImage("UI/Box_Unchecked.png");
                isChecked = false;     
                music.stopMusic();
            }                           
        }
    }    
}
