import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Write a description of class ExampleActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_AI  extends ShipUnloading_CranePlay
{   
    private static final int STATE_IDLE = 0;
    private static final int STATE_PICKUP = 1;
    private static final int STATE_DROPOFF = 2; 
    private static final int STATE_THINK = 3; 
    private int state = STATE_THINK;

    private boolean canPickup = false;
    private boolean canDrop = false;  
    private Coord dropPoint = null;

    private ArrayList<ShipUnloading_ContainerPosition> containerPos = new ArrayList<ShipUnloading_ContainerPosition>();
    private ShipUnloading_ContainerPosition nextContainer;
    private ShipUnloading_Container pickedUp; 

    private ShipUnloading_ShipHalf upperHalf, lowerHalf;
    ShipUnloading_TrafficLight trainLight, truckLight;
    
    Coord truck;
    Coord train;

    public ShipUnloading_AI(){
        setImage("Minigame 2/crane_player.png");
        truck = new Coord(1470, 300);
        train = new Coord(1345, 425);
        setSequences(new Sequence[] {new ContainerSequence(), new MoveSequence(), new ThinkSequence()});
    }

    protected void addedToWorld(World world) {
        this.world = (ShipUnloading) world;   
        craneUpper = new ShipUnloading_CraneUpper();
        getWorld().addObject(craneUpper, 1185, 455);    

        ShipUnloading_ShipHalf[] shipHalf = this.world.getShipHalf();
        this.upperHalf = shipHalf[0];
        this.lowerHalf = shipHalf[1];

        List<ShipUnloading_ContainerPosition> containerPos = world.getObjects(ShipUnloading_ContainerPosition.class);
        for(int i = 0; i < containerPos.size(); i++){
            this.containerPos.add(containerPos.get(i));
        }
        
        ShipUnloading_TrafficLight[] trafficLight = this.world.getAILights();
        this.trainLight = trafficLight[0];
        this.truckLight = trafficLight[1];
    }

    class MoveSequence extends Sequence{
        public void doRun() throws InterruptedException{   
            while (true) {   
                int oldX = getX();
                int oldY = getY();
                if(state == STATE_PICKUP && !canPickup){
                    int dx = nextContainer.getX() - oldX;
                    int dy = nextContainer.getY() - oldY;
                    double distance = Math.sqrt(dx * dx + dy * dy);

                    if(getX() > nextContainer.getX()){
                        setLocation(getX() - 2, getY());
                    }        
                    else if(getX() < nextContainer.getX()){
                        setLocation(getX() + 2, getY());
                    }

                    if(getY() > nextContainer.getY() + 1){
                        craneUpper.setLocation(craneUpper.getX(), getY() - 2);
                        setLocation(getX(), getY() - 2);
                    }     
                    else if(getY() < nextContainer.getY() - 1){
                        craneUpper.setLocation(craneUpper.getX(), getY() + 2);
                        setLocation(getX(), getY() + 2);
                    }       

                    if(distance <= 2)
                    {
                        canPickup = true;                            
                    }

                    if(getY() < 95 || getY() > 810)
                        setLocation(getX(), oldY);

                    if(getX() < 860 || getX() > 1540)
                        setLocation(oldX, getY());                     
                }               
                else if(state == STATE_DROPOFF && !canDrop) {
                    int dx = dropPoint.x - oldX;
                    int dy = dropPoint.y - oldY;
                    double distance = Math.sqrt(dx * dx + dy * dy);   

                    if(getX() > dropPoint.x){
                        setLocation(getX() - 2, getY());
                    }        
                    else if(getX() < dropPoint.x){
                        setLocation(getX() + 2, getY());
                    }

                    if(getY() > dropPoint.y + 1){
                        craneUpper.setLocation(craneUpper.getX(), getY() - 2);
                        setLocation(getX(), getY() - 2);
                    }     
                    else if(getY() < dropPoint.y - 1){
                        craneUpper.setLocation(craneUpper.getX(), getY() + 2);
                        setLocation(getX(), getY() + 2);
                    }   

                    if(distance <= 2){
                        canDrop = true;                          
                    }                    
                }
                waitForNextSequence();
            }
        }
    }

    class ContainerSequence extends Sequence{
        public void doRun() throws InterruptedException{           
            while (true) {
                if(state == STATE_PICKUP && canPickup){                    
                    if(pickedUp == null){
                        ShipUnloading_ContainerPosition topContainer = (ShipUnloading_ContainerPosition)getOneIntersectingObject(ShipUnloading_ContainerPosition.class);
                        if(topContainer != null)
                        {                
                            ShipUnloading_Container container;
                            if((container = topContainer.getTopContainer()) != null)
                            {
                                pickedUp = container;
                                grabContainer(container);
                            }
                        }      

                    }
                }
                else if(state == STATE_DROPOFF && canDrop){
                    ShipUnloading_Truck truck = (ShipUnloading_Truck)getOneIntersectingObject(ShipUnloading_Truck.class);
                    ShipUnloading_Train train = (ShipUnloading_Train)getOneIntersectingObject(ShipUnloading_Train.class);                 
                    if(truck != null && !truck.isLoaded() && pickedUp.getWeight() == 350 && truck.speed == 0){
                        canDrop = false;
                        setState(STATE_IDLE);
                        placeContainer(truck);
                    }
                    else if(train != null && !train.isLoaded() && train.speed == 0){
                        canDrop = false;
                        setState(STATE_IDLE);                        
                        placeContainer(train);
                    }
                }                    
                waitForNextSequence();
            }
        }
    }

    class ThinkSequence extends Sequence{
        public void doRun() throws InterruptedException{           
            while (true) {
                if(truckLight.getColor() != ShipUnloading_TrafficLight.TrafficColor.RED){
                    if(truckLight.getWaitTime() == 0 && world.getTruck() == null){
                        truckLight.setColor(ShipUnloading_TrafficLight.TrafficColor.RED);
                    }
                }
                if(trainLight.getColor() != ShipUnloading_TrafficLight.TrafficColor.RED){
                    if(trainLight.getWaitTime() == 0 && world.getTrain() == null){
                        trainLight.setColor(ShipUnloading_TrafficLight.TrafficColor.RED);
                    }                    
                }
                if(state == STATE_THINK){   
                    if(trainLight.getColor() != ShipUnloading_TrafficLight.TrafficColor.GREEN){
                        ShipUnloading_Transport train = world.getTrain();
                        if(train != null){
                            if(train.isLoaded())
                                trainLight.setColor(ShipUnloading_TrafficLight.TrafficColor.GREEN);
                        }
                    }
                    
                    if(pickedUp == null){
                        boolean preference = false;
                        ShipUnloading_ShipHalf shipHalf = null;
                        int upperWeight = upperHalf.getWeight();
                        int lowerWeight = lowerHalf.getWeight();

                        int totalWeight = upperWeight + lowerWeight;
                        int difference = Math.abs(upperWeight - lowerWeight);

                        if(difference >= 1250){
                            if(upperWeight - lowerWeight > 0){
                                shipHalf = upperHalf;
                                preference = true;
                            }
                            if(upperWeight - lowerWeight < 0){
                                shipHalf = lowerHalf;
                                preference = true;
                            }
                        }

                        Collections.shuffle(containerPos);
                        ListIterator li = containerPos.listIterator();

                        if(!preference){
                            while(li.hasNext()) {            
                                ShipUnloading_ContainerPosition containerPos = (ShipUnloading_ContainerPosition)li.next();
                                if(containerPos.getTopContainer() != null)
                                {       
                                    if(world.getTrain() == null && containerPos.getTopContainer().getWeight() == 500)
                                        continue;
                                        
                                    nextContainer = containerPos;
                                    state = STATE_PICKUP;
                                    break;
                                }
                                else
                                    li.remove();            
                            }    
                        }
                        else{
                            while(li.hasNext()) {            
                                ShipUnloading_ContainerPosition containerPos = (ShipUnloading_ContainerPosition)li.next();
                                if(!containerPos.getIntersect(shipHalf))
                                    continue;

                                if(containerPos.getTopContainer() != null)
                                {           
                                    if(world.getTrain() == null && containerPos.getTopContainer().getWeight() == 500)
                                        continue;
                                        
                                    nextContainer = containerPos;
                                    state = STATE_PICKUP;
                                    break;
                                }
                                else
                                    li.remove();  
                            }
                        }                        
                    }
                    else{
                        if(pickedUp.getWorld() != null){
                            if(pickedUp.getWeight() == 350){
                                if(world.getTruck() != null)
                                    dropPoint = truck;   
                                else if(world.getTrain() != null)
                                    dropPoint = train;
                                else
                                    dropPoint = truck;
                            }
                            else{                            
                                dropPoint = train;                                
                            }
                            state = STATE_DROPOFF;
                        }
                    }
                }
                waitForNextSequence();
            }    
        }
    }

    public void grabContainer(final ShipUnloading_Container grabbedContainer){
        final GameObject_CustomTimer textTimer = new GameObject_CustomTimer(null);
        final Timer timer = new Timer();
        getWorld().addObject(textTimer, getX(), getY() - 50);  
        textTimer.start(3.0d);
        final ShipUnloading_AI crane_AI = this;

        timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    textTimer.substractTime(0.1d);
                    final double timePercent = 3.0d;
                    if(textTimer.getTimeLeft() < ( timePercent * 0.1 )){
                        textTimer.setImage("Timer-4.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.5 )){
                        textTimer.setImage("Timer-3.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.75 )){
                        textTimer.setImage("Timer-2.png");
                    }

                    if(textTimer.getTimeLeft() <= 0){                          
                        textTimer.removeTimer();                     
                        grabbedContainer.setCrane(crane_AI);
                        timer.cancel();
                        canPickup = false;
                        setState(STATE_THINK);
                        world.calculateAIWeight();
                    }     
                }
            }, 100, 100);            
    }

    private void placeContainer(final ShipUnloading_Truck truck){
        final GameObject_CustomTimer textTimer = new GameObject_CustomTimer(null);
        final Timer timer = new Timer();
        getWorld().addObject(textTimer, getX(), getY() - 50);  
        textTimer.start(2.0d);
        timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    textTimer.substractTime(0.1d);
                    final double timePercent = 2.0d;
                    if(textTimer.getTimeLeft() < ( timePercent * 0.1 )){
                        textTimer.setImage("Timer-4.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.5 )){
                        textTimer.setImage("Timer-3.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.75 )){
                        textTimer.setImage("Timer-2.png");
                    }

                    if(textTimer.getTimeLeft() <= 0){                          
                        textTimer.removeTimer();                               
                        truck.setLoaded(true);
                        getWorld().removeObject(pickedUp);
                        pickedUp = null;
                        dropPoint = null;
                        setState(STATE_THINK);
                        timer.cancel();
                        truckLight.setColor(ShipUnloading_TrafficLight.TrafficColor.GREEN);
                    }     
                }
            }, 100, 100);         
    }

    private void placeContainer(final ShipUnloading_Train train){
        final GameObject_CustomTimer textTimer = new GameObject_CustomTimer(null);
        final Timer timer = new Timer();
        getWorld().addObject(textTimer, getX(), getY() - 50);  
        textTimer.start(2.0d);
        timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    textTimer.substractTime(0.1d);
                    final double timePercent = 2.0d;
                    if(textTimer.getTimeLeft() < ( timePercent * 0.1 )){
                        textTimer.setImage("Timer-4.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.5 )){
                        textTimer.setImage("Timer-3.png");
                    }
                    else if(textTimer.getTimeLeft() < ( timePercent * 0.75 )){
                        textTimer.setImage("Timer-2.png");
                    }

                    if(textTimer.getTimeLeft() <= 0){                          
                        textTimer.removeTimer();                              
                        train.loadContainer();
                        getWorld().removeObject(pickedUp);
                        pickedUp = null;
                        dropPoint = null;
                        setState(STATE_THINK);
                        timer.cancel();                        
                    }     
                }
            }, 100, 100);         
    }    

    public void setState(int state){
        this.state = state;
    }
}

