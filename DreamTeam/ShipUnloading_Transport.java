import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ShipUnloading_Transport here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_Transport extends ThreadActor
{
    protected int speed;
    protected boolean loaded;
    protected ShipUnloading world;
    protected int WAIT_POINT;
    protected ShipUnloading_TrafficLight trafficLight;
 
    public ShipUnloading_Transport(ShipUnloading_TrafficLight trafficLight)
    {
        this.loaded = false;
        this.speed = 2;
        this.trafficLight = trafficLight;
        setSequences(new Sequence[] { new MoveSequence()});
    }

    protected void addedToWorld(World world) {
        this.world = (ShipUnloading) world;       
    }
    
    class MoveSequence extends Sequence
    {
        public void doRun() throws InterruptedException
        {        
            while (true) {             
                if(getY() == WAIT_POINT && getLight() == ShipUnloading_TrafficLight.TrafficColor.RED){
                    move(0);
                    speed = 0;
                }                
                else {
                    move(2);
                    speed = 2;
                }
                
                if(isAtEdge()){      
                    spawnTransport();
                    //world.removeObject(getParent());                    
                }
                waitForNextSequence();
            }
        }
    }
    
    public ShipUnloading_Transport getParent(){
        return this;
    }
    
    public int getSpeed(){
        return speed;
    }
    
    public boolean isLoaded(){
        return loaded;
    }
    
    public void setLoaded(boolean loaded){
        this.loaded = loaded;        
    }   
    
    public ShipUnloading_TrafficLight.TrafficColor getLight(){
        return trafficLight.getColor();
    } 
    
    public void spawnTransport(){

    }    
}
