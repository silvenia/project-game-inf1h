import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Building object: one actor that contains five buildings
 * 
 */
public class Security_Building extends Actor
{
    
    /**
     * sets image to its self depending on the enum
     */
    public Security_Building(Enum.BuildingType buildingType){
        switch(buildingType){
            case OFFICE1:{
                setImage("Minigame 4/Office1.png");
                break;
            }
            case WAREHOUSE:{
                setImage("Minigame 4/huis.png");
                break;
            }
            case GATE_WEST:{
                setImage("Minigame 4/GateWest.png");
                break;
            }
            case GATE_EAST:{
                setImage("Minigame 4/GateEast.png");
                break;
            } 
            case WATCHTOWER:{
                setImage("Minigame 4/WatchTower.png");
                break;
            }                 
        }
    }  
}
