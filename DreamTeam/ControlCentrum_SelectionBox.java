import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;  

/**
 * Selectionbox used to visualize which ship has been selected by the player
 */
public class ControlCentrum_SelectionBox extends SmoothMover
{
    private Actor actor;
    private int width, height;
    
    /**
     * drawing of the actual rectangle
     */
    public ControlCentrum_SelectionBox(Actor actor, int width, int height){
        this.actor = actor;
        this.width = width - 2;
        this.height = height - 2;
        setImage(new GreenfootImage(width, height)); 
        getImage().setColor(Color.GREEN);
        getImage().drawLine(0, 0, this.width , 0);         
        getImage().drawLine(0, 0, 0, this.height);  
        getImage().drawLine(this.width, 0, this.width, this.height); 
        getImage().drawLine(0, this.height, this.width, this.height);

    }
    
    /**
     * sets the location of the rectangle to the location given through the parameters.
     */
    public void act() 
    {
        setLocation(actor.getX(), actor.getY());  
    }       
}
