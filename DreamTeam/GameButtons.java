import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GameButtons here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameButtons extends Actor
{
    public enum ButtonState {
        STATE_HOVER,
        STATE_UNHOVER
    }
    
    ButtonState state;
    
    public GameButtons()
    {
        this.state = ButtonState.STATE_UNHOVER;
    }
    /**
     * Act - do whatever the GameButtons wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void mouseHover()
    {
        if (getButtonState() == ButtonState.STATE_UNHOVER && Greenfoot.mouseMoved(this))    
        {      
            setHoverImage();
            setButtonState(ButtonState.STATE_HOVER);  
        }    
        if (getButtonState() == ButtonState.STATE_HOVER && Greenfoot.mouseMoved(null) && ! Greenfoot.mouseMoved(this))    
        {     
            setDefaultImage();
            setButtonState(ButtonState.STATE_UNHOVER);     
        }            
    }
    public ButtonState getButtonState()
    {
        return state;
    }
    
    public void setButtonState(ButtonState state)
    {
        this.state = state;
    }
    
    public void setHoverImage()
    {        
    }
    public void setDefaultImage()
    {
    }    
}
