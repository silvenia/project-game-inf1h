import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.Color;  
import java.awt.Graphics; 

/**
 * Places text on top of the Clock
 * 
 */
public class GameObject_TextTimer extends Actor
{
    private static final Color rgbColor = new Color(0,0,0);
    private static final Font font = new Font("Cooper Black", Font.BOLD, 20);  
    private double target = 0;
    
    /**
     * calls the UpdateTimer
     * 
     */
    public GameObject_TextTimer()
    {
        setImage(new GreenfootImage(50, 32)); 
        updateTimer();
    }  
    
    /**
     * draws the text
     */
    private void updateTimer()
    {
        GreenfootImage image = getImage();  
        image.clear();  
        String s = String.format("%.1f", target);
        image.drawString(s + "s", 1, 16);  
    }
    
    /**
     * sets the time of the timer
     */
    public void setTime(double time)
    {
        target = time;
        updateTimer();
    }
    
    /**
     * subtract time off the current time
     */
    public void substractTime(final double time)
    {
        target -= time;
        updateTimer();
    }
    
    /**
     * returns the remaining time left (double)
     */
    public double getTimeLeft()
    {
        return target;
    }
}
