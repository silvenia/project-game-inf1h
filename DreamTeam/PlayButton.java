import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

/**
 * Write a description of class PlayButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayButton extends MenuActors
{
    
    
    /**
     * Act - do whatever the PlayButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    World world;    
    public PlayButton(World world)
    {
        super();  
        this.world = world;
    }
    public void act() 
    {    
        mouseHover();
        if(Greenfoot.mouseClicked(this))
        {
            World nextWorld = this.world; // create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld); // create curtain object  
            int x = getWorld().getWidth()/2;  
            int y = getWorld().getHeight()/2;  
            getWorld().addObject(fader, x, y); // add curtain into world              
        }
    }  
        
    public void setHoverImage()
    {
        setImage("Menu/MainMenu/PlayButton-hovered.png");
    }
    
    public void setDefaultImage()
    {
        setImage("Menu/MainMenu/PlayButton.png");
    }
}
