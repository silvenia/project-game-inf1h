import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MiniGame3Button here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MiniGame3Button extends MenuActors
{
    /**
     * Act - do whatever the MiniGame3Button wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        mouseHover();
        if(Greenfoot.mouseClicked(this))
        {
             World nextWorld = new GameStart(GameStart.GameType.REPAIR);// create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld); // create curtain object  
            int x = getWorld().getWidth()/2;  
            int y = getWorld().getHeight()/2;  
            getWorld().addObject(fader, x, y); // add curtain into world  
        }
    }    
    
    public void setHoverImage()
    {
        setImage("Menu/GameMenu/MiniGame3-hovered.png");
    }
    
    public void setDefaultImage()
    {
        setImage("Menu/GameMenu/MiniGame3.png");
    }      
}
