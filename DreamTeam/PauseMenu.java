import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Pause menu, accessed by pressing the escape button.
 * pauses the game.
 * 
 */
public class PauseMenu extends World
{
    World world;
    /**
     * Constructor: places buttons and images.
     * 
     */
    public PauseMenu(World world)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1);
        setBackground("Menu/PauseMenu.png");
        
        this.world = world;
                        
        addObject(new MenuObject_Fader(null), getWidth()/2, getHeight()/2);  
        
        int centerX = getWidth() / 2;
        int centerY = getHeight() / 2;
        
        OptionsButton options = new OptionsButton();
        options.setImage("Menu/MainMenu/OptionsButton.png");      
        
        QuitButton quit = new QuitButton();
        quit.setImage("Menu/PauseMenu/QuitButton.png");   
        
        BackButton back = new BackButton(world);
        back.setImage(new GreenfootImage("Menu/BackButton.png"));
        
        addObject(options, centerX, centerY + 15);
        addObject(quit, centerX, centerY + 75);
        addObject(back, centerX, centerY + 255);
    }
    
    /**
     * if the escape key is pressed makes it fade back into the game
     */
    public void act()
    {
        String key = Greenfoot.getKey();  
        if(key!=null && key.equals("escape")) 
        {
            //World nextWorld = new PauseMenu(this); // create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(world); // create curtain object  
            int x = getWidth()/2;  
            int y = getHeight()/2;  
            addObject(fader, x, y); // add curtain into world 
            //Greenfoot.setWorld(world);
        }
    }
    
    public void setWorld(World world)
    {
        this.world = world;
    }
}
