import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class UpgradeMenu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SecurityGame_UpgradeMenu extends Actor
{
    private Security_ButtonUpgrade button[] = new Security_ButtonUpgrade[3];
    private boolean buttonsAdded = false;
    private boolean upgradeDone[] = new boolean[3];
    
    public SecurityGame_UpgradeMenu(boolean upgradeDone[])
    {
        button[0] = new Security_ButtonUpgrade(1);
        button[1] = new Security_ButtonUpgrade(2);
        button[2] = new Security_ButtonUpgrade(3);
        
        this.upgradeDone[0] = upgradeDone[0];
        this.upgradeDone[1] = upgradeDone[1];
        this.upgradeDone[2] = upgradeDone[2];
    
    }
    /**
     * Act - do whatever the UpgradeMenu wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    public void act() 
    {
        addButtons();
    }   
    
    public void addButtons(){
        if(buttonsAdded == false){
            if(!upgradeDone[0])
            getWorld().addObject(button[0], 310, 230);
            if(!upgradeDone[1])
            getWorld().addObject(button[1], 310, 370);
            if(!upgradeDone[2])
            getWorld().addObject(button[2], 310, 500);
            buttonsAdded = true;
        }
    
    }
    public void close(){
        getWorld().removeObject(button[0]);
        getWorld().removeObject(button[1]);
        getWorld().removeObject(button[2]);
        getWorld().removeObject(this);
    }
}
