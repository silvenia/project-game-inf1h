import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List; 

/**
 * Dock/yard object
 */
public class ControlCentrum_Yard extends Actor
{
    private Enum.YardType yardType;
    private Enum.Direction direction;
    private int buildingLevel;
    private String buildingName;
    ControlCentrum_UpgradeButton ub = null;
    
    /**
     * Constructor: changes the image and rotation depending on the place and type of dock/yard
     */
    public ControlCentrum_Yard(Enum.YardType yardType, Enum.Direction direction)
    {
        this.yardType = yardType;
        this.direction = direction;
        this.buildingLevel = 0;
        switch(yardType)
        {
            case YARD_REPAIR:
            {
                this.buildingName = "Repair Yard";
                this.setImage("Minigame 1/Yard_Repair.png");
                break;
            }
            case YARD_CONTAINER:
            {
                this.buildingName = "Container Yard";
                this.setImage("Minigame 1/Yard_Container.png");
                break;
            }
            case YARD_OIL:
            {
                this.buildingName = "Oil Yard";
                this.setImage("Minigame 1/Yard_Oil.png");
                break;
            }
        }
        
        switch(direction)
        {
            case NORTH:
            {
                this.setRotation(270);
                break;
            }
            case WEST:
            {
                getImage().mirrorHorizontally(); 
                break;
            }
            case EAST:
            {
                this.setRotation(0);
                break;
            }
            case SOUTH:
            {
                this.setRotation(90);
                break;
            }
        }
    }
    
    /**
     * returns the building level (upgraded level)
     */
    public int getBuildingLevel(){
        return buildingLevel;
    }
    
    /**
     * sets the building level in case of upgrades
     */
    public void setBuildingLevel(int level){
        buildingLevel = level;
    }
    
    /**
     * adds an star for each upgraded level
     */
    public void checkBuildingLevel(){        
        if(getBuildingLevel() == 1){
            getWorld().addObject(new ControlCentrum_UpgradeStars(), getX() + 5, getY() + 40);
        }
        
        if(getBuildingLevel() == 2){
            getWorld().addObject(new ControlCentrum_UpgradeStars(), getX() + 20, getY() + 40);
        }
    }
    
    public final Enum.YardType getYardType(){
        return this.yardType;
    } 
        
    public void receiveShip(ControlCentrum_Ship selectedShip){
                if(isCompatible(selectedShip))
                {                  
                    ControlCentrum c = (ControlCentrum) getWorld();
                    c.setShipClicked(null);
                                
                }
    }
    
    /**
     * checks whether the ship the player tries to send to the dock/yard is compatible with the current yard/dock type
     */
    public boolean isCompatible(ControlCentrum_Ship selectedShip)
    {
        boolean isCompatible = false;
        switch(yardType)
        {
            case YARD_REPAIR:
            {
                if(selectedShip.getShipState() == Enum.ShipState.DAMAGED)
                {                                        
                    isCompatible = true;
                }
                break;
            }
            case YARD_CONTAINER:
            {
                if((selectedShip.getShipType() == Enum.ShipType.CONTAINER_SMALL
                || selectedShip.getShipType() == Enum.ShipType.CONTAINER_MEDIUM
                || selectedShip.getShipType() == Enum.ShipType.CONTAINER_LARGE) && selectedShip.getShipState() == Enum.ShipState.DAMAGED)
                {
                    isCompatible = true;
                }
                break;
            }
            case YARD_OIL:
            {
                if((selectedShip.getShipType() == Enum.ShipType.OIL_SMALL
                || selectedShip.getShipType() == Enum.ShipType.OIL_MEDIUM
                || selectedShip.getShipType() == Enum.ShipType.OIL_LARGE) && selectedShip.getShipState() == Enum.ShipState.DAMAGED)
                {
                    isCompatible = true;
                }
                break;
            }
        }    
        if(isCompatible)
        {
            switch(direction)
            {
                case WEST:
                {
                    selectedShip.setCoordinates(new Coord(this.getX()-50, this.getY()));
                    break;
                }
                case EAST:
                {
                    selectedShip.setCoordinates(new Coord(this.getX()+50, this.getY()));
                    break;
                }                
            }
            return isCompatible;
        }
        
        return isCompatible;
    }
    
    /**
     * function allows the dock/yard to be upgraded
     */
    public void upgradeBuilding(){
        ControlCentrum c = (ControlCentrum) getWorld();
        
        if(ub == null)
        {
            ub = new ControlCentrum_UpgradeButton(this);
            c.addObject(ub, getX(), getY() - 50);
        }
        else
        {
            c.removeObject(ub);
            ub = null;
        }
    }
   
    /**
     * changes unloadTime depending on the building level
     */
    public double getLoadingTime(double unloadTime){    
        double loadTime = 0;
            switch(buildingLevel)
            {
                case 0: 
                    loadTime = unloadTime * 1.4;
                    //c.getShip().unloadTime *= 1.4;
                    break;
                case 1:
                    loadTime = unloadTime * 1;
                    //c.getShip().unloadTime *= 1;
                    break;
                case 2:
                    loadTime = unloadTime * 0.6;
                    //c.getShip().unloadTime *= 0.6;
                    break;
            }
        
        return loadTime;
    }
    
    public void act(){
        ControlCentrum c = (ControlCentrum) getWorld();
        //checkBuildingLevel();
        if(Greenfoot.mouseClicked(this)) {
            if(c.getShip() == null){
                upgradeBuilding();
            }   
            
            if(c.getShip() != null){
                receiveShip(c.getShip());
            }
        }
    }    
}
