import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.lang.Math.*;

/**
 * Minigame 4: Security
 * 
 */
public class Security extends World
{   
    public GameObject_Scores score = new GameObject_Scores(new Color(255,255,255));
    private GameObject_Clock clock = new GameObject_Clock("Time: ", new Color(255,255,255));
    private GameObject_Funds fund = new GameObject_Funds(new Color(255,255,255));
    private Security_LightRadius light[] = {new Security_LightRadius(null) ,new Security_LightRadius(null),new Security_LightRadius(null)};
    private Security_LightRadius lightPlayer;
    private int difficultyLevel = 0;
    Security_Player player;
    
    private SecurityGame_UpgradeMenu upgradeMenu;
    private boolean menuOpen = false;
    private boolean upgradeDone[] = new boolean[3];
    
    private GameObject_Bar satisfactionBar;
    private Timer spawnTimer;
    public Sound track = Sound.getInstance();
    private Timer boatTimer;
    
    Security_Canvas canvas;
    private List<Security_PathNode> startingPoints = new ArrayList<Security_PathNode>();
    private List<Security_PathNode> finishPoints = new ArrayList<Security_PathNode>();
    
    private Security_BurglarBoat burglarBoat = null;
    
    /**
     * Constructor: Sets background, adds objects to the map, plays music, creates pathfinding and spawns burglars.
     * 
     */
    public Security()
    {    
        // Create a new world with 1920x1080 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("Minigame 4/map-1.png");
        track.setMusic(Sound.MusicTrack.GAME4);
        
        player = new Security_Player();
        //fenceLeftUp = new fenceLeftUp();
        //fenceRightUp = new fenceRightUp();
        //fenceLeftUpSide = new fenceLeftUpSide();
        //fenceRightUpSide = new fenceRightUpSide();
        //fenceLeftBottomSide = new fenceLeftBottomSide();
        //fenceRightBottomSide = new fenceRightBottomSide();
        
        satisfactionBar = new GameObject_Bar("Satisfaction", "% ", 1000, 100);
        addObject(satisfactionBar, 1160, 20);
        addObject(clock, 650, 20);
        addObject(score, 100, 20);
        fund.increaseFunds(10000);
        addObject(fund,  100, 40);
        addObject(player, 600, 600);
        Security_Building tower = new Security_Building(Enum.BuildingType.WATCHTOWER); 
        addObject(tower, 280, 440);
        addObject(light[0], tower.getX(), tower.getY());
        tower = new Security_Building(Enum.BuildingType.WATCHTOWER); 
        
        addObject(tower, 1015, 440);
        addObject(light[1], tower.getX(), tower.getY());
        
        tower = new Security_Building(Enum.BuildingType.WATCHTOWER);
        addObject(tower, 660, 220);
        addObject(light[2], tower.getX(), tower.getY());
        addObject(new Security_Fence(Enum.FenceType.LEFTUP), 56, 382);
        addObject(new Security_Fence(Enum.FenceType.RIGHTUP), 1227, 382);
        addObject(new Security_Fence(Enum.FenceType.LEFTUP_SIDE), 280, 113);
        addObject(new Security_Fence(Enum.FenceType.RIGHTUP_SIDE), 1029, 113);
        addObject(new Security_Fence(Enum.FenceType.LEFTBOTTOM_SIDE), 337, 650);
        addObject(new Security_Fence(Enum.FenceType.RIGHTBOTTOM_SIDE), 974, 650);
        
        addObject(new Security_Building(Enum.BuildingType.GATE_WEST), 633, 650);
        addObject(new Security_Building(Enum.BuildingType.GATE_EAST), 693, 650);
        
        addContainer(Enum.ContainerType.GREEN_SIDE, 190, 360);
        addContainer(Enum.ContainerType.GREEN_SIDE, 190, 500);
                
        addContainer(Enum.ContainerType.RED, 360, 355);
        addContainer(Enum.ContainerType.GREEN, 360, 463);
        addContainer(Enum.ContainerType.RED, 360, 573);
                
        addContainer(Enum.ContainerType.GREEN_SIDE, 505, 405);
        addContainer(Enum.ContainerType.GREEN, 487, 300);
        addContainer(Enum.ContainerType.RED_SIDE, 505, 520);
        
        addContainer(Enum.ContainerType.RED, 790, 280);
        addContainer(Enum.ContainerType.RED_SIDE, 805, 400);
        addContainer(Enum.ContainerType.GREEN, 790, 525);
        
        addContainer(Enum.ContainerType.RED_SIDE, 915, 360);
        addContainer(Enum.ContainerType.GREEN, 898, 495);
        addContainer(Enum.ContainerType.RED_SIDE, 915, 610);
        
        addContainer(Enum.ContainerType.RED, 1110, 355);
        addContainer(Enum.ContainerType.GREEN, 1110, 463);
        addContainer(Enum.ContainerType.RED, 1110, 573);
        
        //Top containers
        addContainer(Enum.ContainerType.RED_SIDE, 555, 180);
        addContainer(Enum.ContainerType.GREEN_SIDE, 760, 160);
        
        addContainer(Enum.ContainerType.RED_SIDE, 425, 150);
        addObject(new Security_Container(Enum.ContainerType.RED_SIDE), 425, 195);
        addContainer(Enum.ContainerType.GREEN, 850, 160);
        
        addObject(new Security_Building(Enum.BuildingType.WAREHOUSE), 150, 190);
        addObject(new Security_Building(Enum.BuildingType.WAREHOUSE), 315, 190);
        addObject(new Security_Building(Enum.BuildingType.WAREHOUSE), 1140, 190);
        addObject(new Security_Building(Enum.BuildingType.WAREHOUSE), 660, 400);
        
        addObject(new Security_Building(Enum.BuildingType.OFFICE1), 976, 190);
        
        lightPlayer = new Security_LightRadius(player);
        addObject(lightPlayer, player.getX(), player.getY());
                
        //spawnBoat();
         createWaypoints();
         spawnBurglar();
         spawnBoat();
        //addObject(new Burglar(), 19, 550);
        
        //addObject(new test(), 387, 200);
        setPaintOrder(GameObject_TextTimer.class, GameObject_CustomTimer.class, Security_ButtonUpgrade.class, SecurityGame_UpgradeMenu.class, Security_PathNode.class, Security_LightRadius.class, Security_Canvas.class, GameObject_Clock.class, GameObject_Funds.class, GameObject_Scores.class, GameObject_Bar.class, Security_Building.class, Security_Fence.class);
    }
    
    /**
     * Changes image of the container depending of the Enum it was given
     */
    private void addContainer(Enum.ContainerType containerType, int x, int y){   
        switch(containerType){
            case GREEN_SIDE:
            case RED_SIDE:{
                addObject(new Security_Container(containerType), x, y);
                addObject(new Security_Container(containerType), x, y + 22);
                break;
            }
            case RED:
            case GREEN:{
                addObject(new Security_Container(containerType), x, y);              
                addObject(new Security_Container(containerType), x + 30, y);
                break;
            }
        }
    }
    
    /**
     *  Upgrade menu-, pause menu and boat functions
     */
    public void act () {
        String key = Greenfoot.getKey();  
        if (key!=null && "u".equals(key))
        { 
            if(menuOpen == true){
                upgradeMenu.close();
                menuOpen = false;
            }
            else{
                upgradeMenu = new SecurityGame_UpgradeMenu(upgradeDone);
                addObject(upgradeMenu,238,364);
                menuOpen = true;
            }
                        
        }              
        if(key!=null && key.equals("escape"))  
        {     
            World nextWorld = new PauseMenu(this); // create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld); // create curtain object  
            int x = getWidth()/2;  
            int y = getHeight()/2;  
            addObject(fader, x, y); // add curtain into world    
            //Greenfoot.setWorld(new PauseMenu(this));
        }  
        
        if(clock.getMinutes() >= 1 && difficultyLevel == 0)
        {
            if(spawnTimer != null)
                spawnTimer.setDelay(15000);
            if(boatTimer != null)
                boatTimer.setDelay(17000);
            difficultyLevel++;
        }      
        
        if(clock.getMinutes() > 15){
            gameOver();
        }
    }
    
    /**
     * calculates the distance between 2 points (pathfinding)
     */
    private double dist(Security_PathNode a, Security_PathNode b) {
        return Math.sqrt( (b.getXcoord() - a.getXcoord())*(b.getXcoord() - a.getXcoord()) + (b.getYcoord() - a.getYcoord())*(b.getYcoord() - a.getYcoord()) );
    }
    
    private void addEdge(Security_PathNode a, Security_PathNode b) {
        double cost = dist(a, b);
        a.addPathEdge(new traversalCost(b, cost));
        b.addPathEdge(new traversalCost(a, cost));
    }
    
    /**
     * creates waypoints for the burglar (hardcoded)
     * 
     * Note: to debug make the commented code at the very bottom active again.
     */    
    private void createWaypoints(){
        Security_PathNode[] pN = new Security_PathNode[70];
        
        pN[0] = new Security_PathNode(19, 332);
        addObject(pN[0], 19, 332);
        pN[1] = new Security_PathNode(19, 550);
        addObject(pN[1], 19, 550);       
        
        pN[2] = new Security_PathNode(200, 700);
        addObject(pN[2], 200, 700);    
        pN[3] = new Security_PathNode(1100, 700);
        addObject(pN[3], 1100, 700);     
        
        pN[4] = new Security_PathNode(1260, 332);
        addObject(pN[4], 1260, 332);
        pN[5] = new Security_PathNode(1260, 550);
        addObject(pN[5], 1260, 550);      
        
        pN[6] = new Security_PathNode(560, 70);
        addObject(pN[6], 560, 70);     
        
        pN[7] = new Security_PathNode(750, 70);
        addObject(pN[7], 750, 70);  
        
        for(int i = 0; i <= 7; i++)
        {
            startingPoints.add(pN[i]);
        }
        
        pN[8] = new Security_PathNode(191, 316);
        addObject(pN[8], 191, 316);    
        pN[9] = new Security_PathNode(191, 442);
        addObject(pN[9], 191, 442);   
        pN[10] = new Security_PathNode(191, 578);
        addObject(pN[10], 191, 578);                  
        
        pN[11] = new Security_PathNode(280, 370);
        addObject(pN[11], 280, 370);   
        pN[12] = new Security_PathNode(280, 512);
        addObject(pN[12], 280, 512);  
        
        pN[13] = new Security_PathNode(330, 304);
        addObject(pN[13], 330, 304);         
        pN[14] = new Security_PathNode(330, 408);
        addObject(pN[14], 330, 408);   
        pN[15] = new Security_PathNode(330, 520);
        addObject(pN[15], 330, 520);    
        pN[16] = new Security_PathNode(330, 624);
        addObject(pN[16], 330, 624);             
        
        pN[17] = new Security_PathNode(420, 304);
        addObject(pN[17], 420, 304);
        pN[18] = new Security_PathNode(420, 408);
        addObject(pN[18], 420, 408);
        pN[19] = new Security_PathNode(420, 520);
        addObject(pN[19], 420, 520);   
        pN[20] = new Security_PathNode(420, 624);
        addObject(pN[20], 420, 624);          
        
        pN[21] = new Security_PathNode(460, 245);
        addObject(pN[21], 460, 245);
        pN[22] = new Security_PathNode(460, 358);
        addObject(pN[22], 460, 358);
        pN[23] = new Security_PathNode(460, 475);
        addObject(pN[23], 460, 475);   
        pN[24] = new Security_PathNode(460, 580);
        addObject(pN[24], 460, 580);      
        
        pN[25] = new Security_PathNode(555, 245);
        addObject(pN[25], 555, 245);
        pN[26] = new Security_PathNode(555, 358);
        addObject(pN[26], 555, 358);
        pN[27] = new Security_PathNode(555, 475);
        addObject(pN[27], 555, 475);   
        pN[28] = new Security_PathNode(555, 580);
        addObject(pN[28], 555, 580);         
        
        pN[29] = new Security_PathNode(505, 140);
        addObject(pN[29], 505, 140);   
        pN[30] = new Security_PathNode(660, 140);
        addObject(pN[30], 660, 140); 
        pN[31] = new Security_PathNode(810, 120);
        addObject(pN[31], 810, 120); 
        
        pN[32] = new Security_PathNode(610, 220);
        addObject(pN[32], 610, 220);   
        pN[33] = new Security_PathNode(710, 220);
        addObject(pN[33], 710, 220); 
        pN[34] = new Security_PathNode(810, 220);
        addObject(pN[34], 810, 220);           
        
        pN[35] = new Security_PathNode(595, 520);
        addObject(pN[35], 595, 520); 
        pN[36] = new Security_PathNode(660, 520);
        addObject(pN[36], 660, 520);  
        pN[37] = new Security_PathNode(720, 520);
        addObject(pN[37], 720, 520);      
        
        pN[38] = new Security_PathNode(755, 345);
        addObject(pN[38], 755, 345); 
        pN[39] = new Security_PathNode(755, 470);
        addObject(pN[39], 755, 470);  
        pN[40] = new Security_PathNode(755, 580);
        addObject(pN[40], 755, 580);    

        pN[41] = new Security_PathNode(855, 230);
        addObject(pN[41], 855, 230); 
        pN[42] = new Security_PathNode(855, 345);
        addObject(pN[42], 855, 345);  
        pN[43] = new Security_PathNode(855, 470);
        addObject(pN[43], 855, 470);  
        pN[44] = new Security_PathNode(855, 570);
        addObject(pN[44], 855, 570);          
        
        pN[45] = new Security_PathNode(900, 310);
        addObject(pN[45], 900, 310);          
        pN[46] = new Security_PathNode(900, 430);
        addObject(pN[46], 900, 430);   
        
        pN[47] = new Security_PathNode(965, 310);
        addObject(pN[47], 965, 310);  
        pN[48] = new Security_PathNode(965, 430);
        addObject(pN[48], 965, 430); 
        pN[49] = new Security_PathNode(965, 560);
        addObject(pN[49], 965, 560);    
        
        pN[50] = new Security_PathNode(1015, 370);
        addObject(pN[50], 1015, 370);  
        pN[51] = new Security_PathNode(1015, 510);
        addObject(pN[51], 1015, 510);   
        
        pN[52] = new Security_PathNode(1070, 305);
        addObject(pN[52], 1070, 305);  
        pN[53] = new Security_PathNode(1070, 410);
        addObject(pN[53], 1070, 410);  
        pN[54] = new Security_PathNode(1070, 520);
        addObject(pN[54], 1070, 520);  
        pN[55] = new Security_PathNode(1070, 620);
        addObject(pN[55], 1070, 620); 

        pN[56] = new Security_PathNode(1175, 305);
        addObject(pN[56], 1175, 305);  
        pN[57] = new Security_PathNode(1175, 410);
        addObject(pN[57], 1175, 410);  
        pN[58] = new Security_PathNode(1175, 520);
        addObject(pN[58], 1175, 520);  
        pN[59] = new Security_PathNode(1175, 620);
        addObject(pN[59], 1175, 620);
        
        pN[60] = new Security_PathNode(375, 345);
        addObject(pN[60], 375, 345);
        pN[61] = new Security_PathNode(375, 455);
        addObject(pN[61], 375, 455);
        
        pN[62] = new Security_PathNode(500, 290);
        addObject(pN[62], 500, 290);
        pN[63] = new Security_PathNode(505, 410);
        addObject(pN[63], 505, 410);
        
        pN[64] = new Security_PathNode(805, 270);
        addObject(pN[64], 805, 270);
        pN[65] = new Security_PathNode(805, 400);
        addObject(pN[65], 805, 400);
        
        pN[66] = new Security_PathNode(915, 360);
        addObject(pN[66], 915, 360);
        pN[67] = new Security_PathNode(915, 485);
        addObject(pN[67], 915, 485);
        
        pN[68] = new Security_PathNode(660, 445);
        addObject(pN[68], 660, 445);
       
        for(int i = 60; i <= 68; i++)
        {
            finishPoints.add(pN[i]);
        }
        //finishPoints.add(pN[15]);
        //finishPoints.add(pN[16]);
        //finishPoints.add(pN[17]);
        pN[69] = new Security_PathNode(660, 300);
        addObject(pN[69], 660, 300);
        
        //Starting points
        addEdge(pN[0], pN[8]);
        addEdge(pN[0], pN[9]);
        addEdge(pN[1], pN[9]);
        addEdge(pN[1], pN[10]);
        
        addEdge(pN[2], pN[10]);
        addEdge(pN[2], pN[16]);
        
        addEdge(pN[3], pN[55]);
        addEdge(pN[4], pN[56]);
        addEdge(pN[4], pN[57]);
        addEdge(pN[5], pN[58]);
        addEdge(pN[5], pN[59]);
        
        addEdge(pN[6], pN[29]);
        addEdge(pN[6], pN[30]);
        addEdge(pN[7], pN[30]);
        addEdge(pN[7], pN[31]);
        
        addEdge(pN[8], pN[11]);
        addEdge(pN[9], pN[11]);
        addEdge(pN[9], pN[12]);
        addEdge(pN[10], pN[12]);
        
        addEdge(pN[13], pN[14]);
        addEdge(pN[14], pN[15]);
        addEdge(pN[15], pN[16]);
        
        addEdge(pN[11], pN[13]);
        addEdge(pN[11], pN[14]);
        addEdge(pN[12], pN[14]);
        addEdge(pN[12], pN[15]);
        
        addEdge(pN[13], pN[17]);   
        addEdge(pN[14], pN[18]);
        addEdge(pN[15], pN[19]);
        addEdge(pN[16], pN[20]);
        
        addEdge(pN[17], pN[18]);
        addEdge(pN[17], pN[21]);
        addEdge(pN[17], pN[22]); 
        addEdge(pN[18], pN[19]);
        addEdge(pN[18], pN[22]);
        addEdge(pN[18], pN[23]);
        addEdge(pN[19], pN[20]);
        addEdge(pN[19], pN[23]);
        addEdge(pN[19], pN[24]);
        addEdge(pN[20], pN[24]);     
        
        addEdge(pN[21], pN[22]);
        addEdge(pN[21], pN[25]);  
        addEdge(pN[22], pN[23]); 
        addEdge(pN[22], pN[26]);  
        addEdge(pN[23], pN[24]);  
        addEdge(pN[23], pN[27]);  
        addEdge(pN[24], pN[28]);  
        
        addEdge(pN[25], pN[26]); 
        addEdge(pN[26], pN[27]); 
        addEdge(pN[27], pN[28]); 
        addEdge(pN[27], pN[35]);          
        addEdge(pN[28], pN[35]);  
        
        addEdge(pN[29], pN[21]);
        addEdge(pN[29], pN[30]);
        addEdge(pN[30], pN[31]);
        addEdge(pN[30], pN[32]);
        addEdge(pN[30], pN[33]);
        addEdge(pN[31], pN[34]);
        
        addEdge(pN[32], pN[25]);
        addEdge(pN[33], pN[34]);
        addEdge(pN[33], pN[38]);
        
        addEdge(pN[34], pN[41]);
        
        addEdge(pN[35], pN[36]);
        
        addEdge(pN[36], pN[37]);
        //addEdge(pN[36], pN[68]);
        pN[36].addPathEdge(new traversalCost(pN[68], dist(pN[36], pN[68])));
        pN[68].addPathEdge(new traversalCost(pN[69], dist(pN[68], pN[69])));
        pN[69].addPathEdge(new traversalCost(pN[32], dist(pN[69], pN[32])));
        pN[69].addPathEdge(new traversalCost(pN[33], dist(pN[69], pN[33])));
        addEdge(pN[37], pN[39]);
        addEdge(pN[37], pN[40]);
        
        addEdge(pN[38], pN[39]);
        addEdge(pN[38], pN[42]);
        addEdge(pN[39], pN[40]);
        
        addEdge(pN[39], pN[43]);
        addEdge(pN[40], pN[44]);
        
        addEdge(pN[41], pN[42]);
        addEdge(pN[41], pN[45]);
        addEdge(pN[42], pN[43]);
        addEdge(pN[42], pN[45]);
        addEdge(pN[43], pN[44]);
        addEdge(pN[43], pN[46]);
        addEdge(pN[44], pN[49]);
        
        addEdge(pN[45], pN[47]);
        addEdge(pN[46], pN[48]);
              
        addEdge(pN[47], pN[48]);
        addEdge(pN[47], pN[50]);
        addEdge(pN[47], pN[52]);
        addEdge(pN[48], pN[49]);
        addEdge(pN[48], pN[50]);
        addEdge(pN[48], pN[51]);
        addEdge(pN[49], pN[51]);
        addEdge(pN[49], pN[55]);
        
        addEdge(pN[50], pN[52]);
        addEdge(pN[50], pN[53]);
        addEdge(pN[51], pN[53]);
        addEdge(pN[51], pN[54]);
        
        addEdge(pN[52], pN[53]);
        addEdge(pN[52], pN[56]);
        addEdge(pN[53], pN[54]);
        addEdge(pN[53], pN[57]);
        addEdge(pN[54], pN[55]);
        addEdge(pN[54], pN[58]);
        addEdge(pN[55], pN[59]);
        
        addEdge(pN[56], pN[57]);
        addEdge(pN[57], pN[58]);
        addEdge(pN[58], pN[59]);
        
        pN[18].addPathEdge(new traversalCost(pN[60], dist(pN[18], pN[60])));
        pN[60].addPathEdge(new traversalCost(pN[17], dist(pN[60], pN[17])));
        pN[19].addPathEdge(new traversalCost(pN[61], dist(pN[19], pN[61])));
        pN[61].addPathEdge(new traversalCost(pN[18], dist(pN[61], pN[18])));
        pN[26].addPathEdge(new traversalCost(pN[62], dist(pN[26], pN[62])));
        pN[62].addPathEdge(new traversalCost(pN[25], dist(pN[62], pN[25])));
        pN[27].addPathEdge(new traversalCost(pN[63], dist(pN[27], pN[63])));
        pN[63].addPathEdge(new traversalCost(pN[26], dist(pN[63], pN[26])));
        
        pN[38].addPathEdge(new traversalCost(pN[64], dist(pN[38], pN[64])));
        pN[64].addPathEdge(new traversalCost(pN[33], dist(pN[64], pN[33])));
        pN[39].addPathEdge(new traversalCost(pN[65], dist(pN[39], pN[65])));
        pN[65].addPathEdge(new traversalCost(pN[38], dist(pN[65], pN[38])));
        pN[46].addPathEdge(new traversalCost(pN[66], dist(pN[46], pN[66])));
        pN[66].addPathEdge(new traversalCost(pN[45], dist(pN[66], pN[45])));
        pN[44].addPathEdge(new traversalCost(pN[67], dist(pN[44], pN[67])));
        pN[67].addPathEdge(new traversalCost(pN[46], dist(pN[67], pN[46])));
        
        /*canvas = new canvas(getWidth(), getHeight());
        addObject(canvas, getWidth()/2, getHeight()/2);        
        for(Security_PathNode PathNode1 : pN) {
            for(traversalCost PathNode2 : PathNode1.getPathEdges()) {
                canvas.drawLine(PathNode1, PathNode2.getNode(), Color.black);
                canvas.drawArrows(PathNode1, PathNode2.getNode(), Color.white);
                canvas.drawText(PathNode1, PathNode2.getNode(), Color.white);
                /*
                int x1 = PathNode1.getXcoord();
                int y1 = PathNode1.getYcoord();
                int x2 = PathNode2.getNode().getXcoord();
                int y2 = PathNode2.getNode().getYcoord();
                canvasImg.setColor(Color.black);
                canvasImg.drawLine(x1, y1, x2, y2);
                drawArrows(x1, y1, x2, y2, canvasImg);
                
            }
        }*/
    }
    
    public int getSatisfaction(){
        return satisfactionBar.getValue();
    }
    
    public void reduceSatisfaction(int amount){
        satisfactionBar.subtract(amount);
        
        if(getSatisfaction() < 50)
            gameOver();        
    }
    
    public void addSatisfaction(int amount){
        satisfactionBar.add(amount);
    }
    
    public int getFunds(){
        return fund.getFunds();
    }
    
    public void increaseFunds(int amount){
        fund.increaseFunds(amount);
    }
    
    public void decreaseFunds(int amount){
        fund.decreaseFunds(amount);
    }
    public void increaseSpeed(){
        player.increaseSpeed();
        markUpgradeDone(1);
        
    }
    public void updateTowerRadius(){
        light[0].setRadius(250);
        light[1].setRadius(250);
        light[2].setRadius(250);
        markUpgradeDone(2);
    }
    public void updatePlayerRadius(){
        lightPlayer.setRadius(180);
        markUpgradeDone(3);
    }
    
    public void markUpgradeDone(int type){
        switch(type){
            case 1:
                upgradeDone[0] = true;
                break;
            case 2:
                upgradeDone[1] = true;
                break;
            case 3:
                upgradeDone[2] = true;
                break;  
        }
    }
    
    /**
     * spawns the burglar randomly: random start point and random end point
     */
    public void spawnBurglar (){
         ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                int spawnPosition = Greenfoot.getRandomNumber(6);
                int finishPosition = Greenfoot.getRandomNumber(9);
                addObject(new Security_Burglar(getStartingPoint().get(spawnPosition), getFinishPoint().get(finishPosition)), getStartingPoint().get(spawnPosition).getX(), getStartingPoint().get(spawnPosition).getY());
                //addObject(new Thief(spawnedBottom, xSpawn, ySpawn),xSpawn, ySpawn);
               
            }
        };
        spawnTimer = new Timer(20*1000, taskPerformer);   
        spawnTimer.setInitialDelay(5*1000);
        spawnTimer.start();
    }
    
    /**
     * shows the gameover screen
     */
        public void gameOver(){
            Greenfoot.setWorld(new GameOver("Security", score.getScore()));
        
    }
    
    /**
     * spawns a boat, makes the boat leave whenever the burglar is aboard again or after x seconds have passed
     */
    private void spawnBoat(){
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {       
                if(burglarBoat == null)
                {
                    burglarBoat = new Security_BurglarBoat();
                    addObject(burglarBoat, 1, 22);    
                }
            }
        };
        
        boatTimer = new Timer(20*1000, taskPerformer);   
        boatTimer.setInitialDelay(10*1000);
        boatTimer.start();
    }
    
    public List<Security_PathNode> getStartingPoint(){
        return startingPoints;
    }
        
    public List<Security_PathNode> getFinishPoint(){
        return finishPoints;
    }       
    
    public Security_BurglarBoat getBoat(){
        return burglarBoat;
    }
    public void removeBoat(){
        burglarBoat = null;
    }    
}
