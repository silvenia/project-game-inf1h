import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Write a description of class ExampleActor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_Train extends ShipUnloading_Transport
{
    int numOfContainers;
    
    public ShipUnloading_Train(ShipUnloading_TrafficLight trafficLight)
    {
        super(trafficLight);
        setImage("Minigame 2/Train.png");
        turn(270);
        this.numOfContainers = 0;
        this.WAIT_POINT = 455;
    }        
    
    public void loadContainer(){
        numOfContainers++;
        if(numOfContainers == 1){
            setImage("Minigame 2/Train_loaded1.png");
        }

        else if(numOfContainers == 2){
            setImage("Minigame 2/Train_loaded2.png");
        }

        else if(numOfContainers == 3){
            setImage("Minigame 2/Train_loaded3.png");
            setLoaded(true);
        }        
    }
    public void spawnTransport(){
        int location = 0;
        
        if(getX() > 700)
            location = ShipUnloading.LOCATION_AI;
        else
            location = ShipUnloading.LOCATION_PLAYER;
        world.spawnTransport(this, ShipUnloading.TRANSPORT_TRAIN, location);
    }    
}
