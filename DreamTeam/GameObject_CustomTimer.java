import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * 'clock' used to visualize the remaining time of a task
 */
public class GameObject_CustomTimer extends Actor
{
    GameObject_TextTimer timer;
    Actor actor;
    
    /**
     * Constructor: sets image to initial 100%
     */
    public GameObject_CustomTimer(Actor actor)
    {
        this.setImage("Timer-1.png");
        if(actor != null)
            this.actor = actor;
        //this.ship = ship;
        //this.timer = new Timer();
        //this.getWorld().addObject(timer, 250, 250);
        //prepare();
    }
    
    /**
     * places the text inside the clock
     */
    public void act() 
    {
        if(actor != null)
        {
            timer.setLocation(getX() + 15, getY() + 5);
            setLocation(actor.getX(), actor.getY() - 50);
        }
        // Add your action code here.        
    }    
    
    /**
     * triggers when its added to the world: adds the clock the world
     */
    protected void addedToWorld(World world)
    {
        timer = new GameObject_TextTimer();
        getWorld().addObject(timer, getX() + 15, getY() + 5);
    }
    
    /**
     * starts the timer for the amount of time given in the parameter
     */
    public void start(double time)
    {
        timer.setTime(time);
    }
    
    /**
     * subtracts for the amount given in the parameter
     */
    public void substractTime(double time)
    {
        timer.substractTime(time);
    }
    
    /**
     * returns the amount of time left (double)
     */
    public double getTimeLeft()
    {
        return timer.getTimeLeft();
    }
    
    /**
     * removes the text and this clock from the world
     */
    public void removeTimer()
    {   
        getWorld().removeObject(timer);
        getWorld().removeObject(this);
    }
}
