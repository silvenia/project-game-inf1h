/*
    This is an exapmle of an A* Pathfinding Algorithm written in Java/Greenfoot for my educational purposes and to prototype for future useage in games
    Copyright (C) 2011 Shaun Steenkamp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * Each nodeSearch represents a particular node.
 * 
 * It stores the cost it has taken to get here so far and the estimated cost to the finish.
 * 
 * @author Shaun S.
 * @version 0.5 Alpha 290911
 */
public class nodeSearch  
{
    // the node on the grid that we represent
    public Security_PathNode node;
    // the parent node
    private nodeSearch parent;
    // the cost to get here so far
    private double currCost;
    // the estimated cost to the finish
    private double FVal;
    
    public nodeSearch(Security_PathNode nodemap, double cost, double f, nodeSearch par) {
        node = nodemap;
        currCost = cost;
        FVal = f;
        parent = par;
    }
    
    public Security_PathNode getNode() {
        return node;
    }
    public nodeSearch getParent() {
        return parent;
    }
    public double getCost() {
        return currCost;
    }
    public double getFVal() {
        return FVal;
    }
}