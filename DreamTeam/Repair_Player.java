import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Player object that is controlled by the actual player
 */
public class Repair_Player extends SmoothMover
{
    Repair_Tools wrench;
    Repair_Tools brush;
    Repair_Tools drill;
    Repair_Tools screw;
    Repair_BrokenContainer brokenContainer;
    boolean isHoldingTool = false;
    boolean isHoldingWrench = false;
    boolean isHoldingBrush = false;
    boolean isHoldingDrill = false;
    boolean isHoldingScrew = false;
    boolean eKeyPresent = false;
    int counter = 0;
    
    /**
     * - manages the tools 
     * - calls different functions that check whether different actions have been performed
     */
    public void act(){
        Repair world = (Repair) getWorld();
        List<Repair_Tools> tools = world.getObjects(Repair_Tools.class);
        for(int i = 0; i < tools.size(); i++){
            if(tools.get(i).getType() == Repair_Tools.ToolType.DRILL){
                drill = tools.get(i);
            }
            else if(tools.get(i).getType() == Repair_Tools.ToolType.SCREWDRIVER){
                screw = tools.get(i);
            }
        
        }
        moveAndTurn();
        isInRangeOfTool();
        checkImages();
        layDownTool();
        pickContainer();
        repairContainer();
        counter++;
        List containerList = getObjectsInRange(100, Repair_BrokenContainer.class);
        if(!containerList.isEmpty()){
            brokenContainer = (Repair_BrokenContainer) containerList.get(0);
        }
    }  
    
    /**
     * constructor: retrieves parameter data
     */
    public Repair_Player(Repair_Tools wrench, Repair_Tools brush){
        setImage("Minigame 3/RepairPlayer1.png");
        this.wrench = wrench;
        this.brush = brush;
        //this.bk = bk;
    }
    
    public void moveAndTurn(){
        if(Greenfoot.isKeyDown("up")){
            move(6);
        }
        
        if(Greenfoot.isKeyDown("down")){
            move(-3);
        }
        
        if(Greenfoot.isKeyDown("left")){
            turn(-3);
        }
        
        if(Greenfoot.isKeyDown("right")){
            turn(3);
        }
        
    }
    
    public void checkImages(){
        if(this.getObjectsInRange(2000, Repair_Ekey.class).size() >= 1){
            eKeyPresent = true;
        }
        
        else {
            eKeyPresent = false;
        }
        
        if(isHoldingTool){
            setImage("Minigame 3/RepairPlayer_Tool.png");
        }
        
        else {
            setImage("Minigame 3/RepairPlayer1.png");
        }
    }
    
    public void isInRangeOfTool(){
        Repair world = (Repair) getWorld();
        List<Repair_Tools> tools = getObjectsInRange(50, Repair_Tools.class);
        if(!tools.isEmpty()){
                switch(tools.get(0).getType()){
                    case DRILL:
                        pickUpTool("drill");
                        break;
                    case SCREWDRIVER:
                        pickUpTool("screwdriver");
                        break;
                    case WRENCH:
                        pickUpTool("wrench");
                        break; 
                    case BRUSH:
                        pickUpTool("brush");
                        break;
                }
                
        }
        else{
            world.removeObjects(world.getObjects(Repair_Ekey.class));
            eKeyPresent = false;
        
        }
    }
    
    public void pickUpTool(String tool){
        Repair world = (Repair) getWorld();
        if(tool.equals("wrench")){
            if(!eKeyPresent){
                world.addObject(new Repair_Ekey(), wrench.getX(), wrench.getY() + 50);
            }
            
            if(Greenfoot.isKeyDown("e") && !isHoldingTool){
                isHoldingWrench = true;
                isHoldingTool = true;
                world.removeObject(wrench);
                world.removeObjects(world.getObjects(Repair_Ekey.class));
            }
        }
        
        if(tool.equals("brush")){
            if(!eKeyPresent){
                world.addObject(new Repair_Ekey(), brush.getX(), brush.getY() + 50);
            }
            
            if(Greenfoot.isKeyDown("e") && !isHoldingTool){
                isHoldingBrush = true;
                isHoldingTool = true;
                world.removeObject(brush);
                world.removeObjects(world.getObjects(Repair_Ekey.class));
            }
        }
        
        if(tool.equals("drill")){
            if(!eKeyPresent && drill.getUseable()){
                world.addObject(new Repair_Ekey(), drill.getX(), drill.getY() + 50);
            }
            
            if(Greenfoot.isKeyDown("e") && !isHoldingTool && drill.getUseable()){
                isHoldingDrill = true;
                isHoldingTool = true;
                world.removeObject(drill);
                world.removeObjects(world.getObjects(Repair_Ekey.class));
            }
        }
        
        if(tool.equals("screwdriver")){
            if(!eKeyPresent && screw.getUseable()){
                world.addObject(new Repair_Ekey(), screw.getX(), screw.getY() + 50);
            }
            
            if(Greenfoot.isKeyDown("e") && !isHoldingTool && screw.getUseable()){
                isHoldingScrew = true;
                isHoldingTool = true;
                world.removeObject(screw);
                world.removeObjects(world.getObjects(Repair_Ekey.class));
            }
        }
        
    }
    
    public void layDownTool(){
        Repair world = (Repair) getWorld();
        if(isHoldingWrench && Greenfoot.isKeyDown("t") && counter >= 90){
            world.addObject(wrench, getX(), getY());
            isHoldingWrench = false;
            isHoldingTool = false;
            counter = 0;
        }
        
        if(isHoldingBrush && Greenfoot.isKeyDown("t") && counter >= 90){
            world.addObject(brush, getX(), getY());
            isHoldingBrush = false;
            isHoldingTool = false;
            counter = 0;
        }
        
        if(isHoldingDrill && Greenfoot.isKeyDown("t") && counter >= 90){
            world.addObject(drill, getX(), getY());
            isHoldingDrill = false;
            isHoldingTool = false;
            counter = 0;
        }
        
        if(isHoldingScrew && Greenfoot.isKeyDown("t") && counter >= 90){
            world.addObject(screw, getX(), getY());
            isHoldingScrew = false;
            isHoldingTool = false;
            counter = 0;
        }
    }
    
    public void pickContainer(){
        if(!this.getObjectsInRange(50, Repair_BrokenContainer.class).isEmpty() && !isHoldingTool){
            brokenContainer.chooseThis();
        }
    }

   
    public void repairContainer(){
        if(!this.getObjectsInRange(50, Repair_BrokenContainer.class).isEmpty()){
            if((brokenContainer.getToolNeeded().equals("wrench") && isHoldingWrench) || (brokenContainer.getToolNeeded().equals("brush") && isHoldingBrush) || (brokenContainer.getToolNeeded().equals("drill") && isHoldingDrill) || (brokenContainer.getToolNeeded().equals("screw") && isHoldingScrew)){
                if(Greenfoot.isKeyDown("e") && !brokenContainer.timerIsMade && brokenContainer.isBroken){
                    brokenContainer.updateRepairTime();
                }
            }
        }
        
    }
}
