import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;  
import java.util.*;
import java.util.Timer;

/**
 * Write a description of class ShipUnloading here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading extends World{
    ShipUnloading_Player player;
    ShipUnloading_ShipHalf upperHalf, lowerHalf, upperHalf2, lowerHalf2;
    GameObject_CustomText upperText, lowerText, upperText2, lowerText2;
    
    ShipUnloading_SinkShip sinkShip, sinkShip2;
    ShipUnloading_TrafficLight playerLight[] = {new ShipUnloading_TrafficLight(), new ShipUnloading_TrafficLight()};
    ShipUnloading_TrafficLight AILight[] = {new ShipUnloading_TrafficLight(), new ShipUnloading_TrafficLight()};        
    
    ShipUnloading_Truck AITruck;
    ShipUnloading_Train AITrain;
    
    private GameObject_Clock clock = new GameObject_Clock("Time: ", new Color(255,255,255));
    
    public static final int LOCATION_PLAYER = 0;
    public static final int LOCATION_AI = 1;
    public static final int TRANSPORT_TRUCK = 0;
    public static final int TRANSPORT_TRAIN = 1;    
    
    public void act(){                          
    }
    
    /**
     * Constructor for objects of class ShipUnloading.
     * 
     */
    public ShipUnloading(){    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1600, 900, 1);         
        setBackground("Minigame 2/map.png");
        prepare();
        setPaintOrder(GameObject_TextFloat.class, GameObject_TextTimer.class, GameObject_CustomTimer.class, ShipUnloading_ContainerPosition.class, ShipUnloading_ShipHalf.class, ShipUnloading_CraneUpper.class, ShipUnloading_Player.class, ShipUnloading_AI.class, ShipUnloading_TrafficLight.class, ShipUnloading_Crane.class, GameObject_CustomText.class, ShipUnloading_Container.class, ShipUnloading_Ship.class, ShipUnloading_Truck.class);
    }    
    
    private void prepare(){
 
        addObject(new ShipUnloading_Ship(), 642, 450);
        addObject(new ShipUnloading_Ship(), 950, 450);        
        
        addObject(new ShipUnloading_Truck(playerLight[0]), 126, 890);
        
        addObject(new ShipUnloading_Train(playerLight[1]), 257, 845);
        
        AITruck = new ShipUnloading_Truck(AILight[1]);
        addObject(AITruck, 1470, 890);
        
        AITrain = new ShipUnloading_Train(AILight[0]);
        addObject(AITrain, 1345, 845);            
        //ShipUnloading_Train train2 = new ShipUnloading_Train();
        //addObject(train2, 1345, 845);        
        
        ShipUnloading_Crane crane = new ShipUnloading_Crane();
        addObject(crane, 410, 525);   

        ShipUnloading_Crane crane_AI = new ShipUnloading_Crane();
        addObject(crane_AI, 1185, 525);         
        
        lowerHalf = new ShipUnloading_ShipHalf();
        addObject(lowerHalf, 645, 656); 
        
        upperHalf = new ShipUnloading_ShipHalf();
        addObject(upperHalf, 645, 394);                     
        
        lowerHalf2 = new ShipUnloading_ShipHalf();
        addObject(lowerHalf2, 953, 656); 
        
        upperHalf2 = new ShipUnloading_ShipHalf();
        addObject(upperHalf2, 953, 394);         
        
        addObject(playerLight[0], 30, 160); 
        addObject(playerLight[1], 192, 160); 
        addObject(AILight[0], 1410, 160); 
        addObject(AILight[1], 1570, 160);         
        //spawnContainers();
        
        //AI's Containers
        addObject(new ShipUnloading_ContainerPosition(), 890, 330);
        addObject(new ShipUnloading_ContainerPosition(), 955, 330);
        addObject(new ShipUnloading_ContainerPosition(), 1020, 330);
        
        addObject(new ShipUnloading_ContainerPosition(), 890, 455);
        addObject(new ShipUnloading_ContainerPosition(), 955, 455);
        addObject(new ShipUnloading_ContainerPosition(), 1020, 455);
        
        addObject(new ShipUnloading_ContainerPosition(), 890, 595);
        addObject(new ShipUnloading_ContainerPosition(), 955, 595);
        addObject(new ShipUnloading_ContainerPosition(), 1020, 595);
        
        addObject(new ShipUnloading_ContainerPosition(), 890, 720);
        addObject(new ShipUnloading_ContainerPosition(), 955, 720);
        addObject(new ShipUnloading_ContainerPosition(), 1020, 720);
        
        ShipUnloading_AI AI = new ShipUnloading_AI();
        addObject(AI, 1185, 456);
              
        addObject(new ShipUnloading_ContainerPosition(), 580, 330);
        addObject(new ShipUnloading_ContainerPosition(), 645, 330);
        addObject(new ShipUnloading_ContainerPosition(), 710, 330);
        
        addObject(new ShipUnloading_ContainerPosition(), 580, 455);
        addObject(new ShipUnloading_ContainerPosition(), 645, 455);
        addObject(new ShipUnloading_ContainerPosition(), 710, 455);    
        
        addObject(new ShipUnloading_ContainerPosition(), 580, 595);
        addObject(new ShipUnloading_ContainerPosition(), 645, 595);
        addObject(new ShipUnloading_ContainerPosition(), 710, 595);
        
        addObject(new ShipUnloading_ContainerPosition(), 580, 720);
        addObject(new ShipUnloading_ContainerPosition(), 645, 720);
        addObject(new ShipUnloading_ContainerPosition(), 710, 720);          
        
        player = new ShipUnloading_Player();
        addObject(player, 410, 456);   
        
        upperText = new GameObject_CustomText("0 kg", new Color(255, 255, 255));
        addObject(upperText, upperHalf.getX(), upperHalf.getY() - 130);
        lowerText = new GameObject_CustomText("0 kg", new Color(255, 255, 255));
        addObject(lowerText, lowerHalf.getX(), lowerHalf.getY() + 130);
        
        upperText2 = new GameObject_CustomText("0 kg", new Color(255, 255, 255));
        addObject(upperText2, upperHalf2.getX(), upperHalf2.getY() - 130);
        lowerText2 = new GameObject_CustomText("0 kg", new Color(255, 255, 255));
        addObject(lowerText2, lowerHalf2.getX(), lowerHalf2.getY() + 130);        
        
        ShipUnloading_SinkBg sinkBg = new ShipUnloading_SinkBg();
        addObject(sinkBg, 402, 135); 
        
        sinkShip = new ShipUnloading_SinkShip();
        addObject(sinkShip, 403, 130);     
        
        ShipUnloading_SinkBg sinkBg2 = new ShipUnloading_SinkBg();
        addObject(sinkBg2, 1198, 135); 
        
        sinkShip2 = new ShipUnloading_SinkShip();
        addObject(sinkShip2, 1199, 130);            
        
        addObject(clock, 775, 20);
        
        calculatePlayerWeight();
        calculateAIWeight();
    }               
    
    public void calculatePlayerWeight(){
        int upperWeight = upperHalf.calculateWeight();
        int lowerWeight = lowerHalf.calculateWeight();
                
        upperText.setText(Integer.toString(upperWeight) + " kg");
        lowerText.setText(Integer.toString(lowerWeight) + " kg");
        
        int totalWeight = upperWeight + lowerWeight;
        int difference = Math.abs(upperWeight - lowerWeight);
        
        if(totalWeight == 0){
            Greenfoot.setWorld(new GameOver("ShipUnloading (You won)", (int)clock.getSeconds()));
            return;
        }
        
        if(difference > 0 && totalWeight > 1000){            
            float sinkValue = ((float)difference / (float)totalWeight) * 100;
            if(upperWeight - lowerWeight > 0){                
                sinkShip.setRotation((int)sinkValue);
            }
            else if(upperWeight - lowerWeight < 0){
                sinkShip.setRotation(360 - (int)sinkValue);
            }
            if(sinkValue > 25)
                Greenfoot.setWorld(new GameOver("ShipUnloading (AI won)", (int)clock.getSeconds()));            
        }
        else if(totalWeight <= 1000)
            sinkShip.setRotation(0);    
                    
    }
    
    public void calculateAIWeight(){
        int upperWeight = upperHalf2.calculateWeight();
        int lowerWeight = lowerHalf2.calculateWeight();
        
        upperText2.setText(Integer.toString(upperWeight) + " kg");
        lowerText2.setText(Integer.toString(lowerWeight) + " kg");
        
        int totalWeight = upperWeight + lowerWeight;
        int difference = Math.abs(upperWeight - lowerWeight);

        if(totalWeight == 0){
            Greenfoot.setWorld(new GameOver("ShipUnloading (AI won)", (int)clock.getSeconds()));
            return;
        }
        
        if(difference > 0 && totalWeight > 1000)
        {            
            float sinkValue = ((float)difference / (float)totalWeight) * 100;
            if(upperWeight - lowerWeight > 0)
            {                
                sinkShip2.setRotation((int)sinkValue);
            }
            else if(upperWeight - lowerWeight < 0)
            {
                sinkShip2.setRotation(360 - (int)sinkValue);
            }
            if(sinkValue > 25)
                Greenfoot.setWorld(new GameOver("ShipUnloading (You won)", (int)clock.getSeconds()));
        }
        else if(totalWeight <= 1000)
            sinkShip2.setRotation(0);  
            
            
    }
    
    public ShipUnloading_ShipHalf[] getShipHalf()
    {
        ShipUnloading_ShipHalf shipHalf[] = {upperHalf2, lowerHalf2};
        return shipHalf; 
    }
    
    public void spawnTransport(ShipUnloading_Transport transport, final int transportType, final int location){    
        removeObject(transport);        
        Timer timer = new Timer();
        int time = 10000;
        if(transportType == TRANSPORT_TRUCK)
            time = 12000;
        else if(transportType == TRANSPORT_TRAIN)
            time = 20000;
            
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(transportType == TRANSPORT_TRUCK)
                {
                    if(location == LOCATION_PLAYER)
                        addObject(new ShipUnloading_Truck(playerLight[0]), 126, 890);
                    else if(location == LOCATION_AI)
                    {
                        AITruck = new ShipUnloading_Truck(AILight[1]);
                        addObject(AITruck, 1470, 890);
                    }
                }
                else if(transportType == TRANSPORT_TRAIN){
                    if(location == LOCATION_PLAYER)
                        addObject(new ShipUnloading_Train(playerLight[1]), 257, 845);
                    else if(location == LOCATION_AI)
                    {
                        AITrain = new ShipUnloading_Train(AILight[0]);
                        addObject(AITrain, 1345, 845);    
                    }
                }                
            }
        }, time);    
    }
    
    public ShipUnloading_TrafficLight[] getAILights(){
        ShipUnloading_TrafficLight[] trafficLight = {AILight[0], AILight[1]};
        return trafficLight;
    }
    
    public ShipUnloading_Truck getTruck(){
        if(AITruck.getWorld() != null)
            return AITruck;
            
        return null;
    }
    public ShipUnloading_Train getTrain(){
        if(AITrain.getWorld() != null)
            return AITrain;
            
        return null;
    }    
}
