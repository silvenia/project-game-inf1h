import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class UpgradeButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ControlCentrum_UpgradeButton extends GameButtons
{

    public ControlCentrum_UpgradeButton(ControlCentrum_Yard yard){
        this.yard = yard;
    }

    private ControlCentrum_Yard yard;
    private boolean clicked;

    public void act(){
        mouseHover();
        if(Greenfoot.mouseClicked(this)){
            upgrade();
        }
    }

    public void upgrade(){
        final ControlCentrum c = (ControlCentrum) getWorld();
        int money = c.funding.getFunds();

        if(yard.getBuildingLevel() == 0 && money >= 25000){
            yard.setBuildingLevel(1);
            c.funding.decreaseFunds(25000);
            getWorld().removeObject(this);
            yard.checkBuildingLevel();
        }
        else if(yard.getBuildingLevel() == 1 && money >= 50000){
            yard.setBuildingLevel(2);
            c.funding.decreaseFunds(50000);
            getWorld().removeObject(this);
            yard.checkBuildingLevel();
        } else if(yard.getBuildingLevel() == 2){
            getWorld().removeObject(this);

        }
    }

    public void setHoverImage()
    {
        setImage("images/UpgradeButton-hovered.png");
    }

    public void setDefaultImage()
    {
        setImage("images/UpgradeButton2.png");
    }        
}