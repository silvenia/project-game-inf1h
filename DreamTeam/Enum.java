public final class Enum {
    public enum BuildingType{
        OFFICE1,
        WAREHOUSE, 
        GATE_WEST,
        GATE_EAST,
        WATCHTOWER
    }        
    public enum ContainerType{
        RED,
        RED_SIDE,
        GREEN,
        GREEN_SIDE,      
    }    
    public enum FenceType{
        LEFTUP,
        RIGHTUP,
        LEFTUP_SIDE,
        RIGHTUP_SIDE,
        LEFTBOTTOM_SIDE,
        RIGHTBOTTOM_SIDE        
    }
    public enum ShipType 
    {
        CONTAINER_SMALL,
        CONTAINER_MEDIUM,
        CONTAINER_LARGE,
        OIL_SMALL,
        OIL_MEDIUM,
        OIL_LARGE,
        BOAT
    }
    public enum ShipState
    {
        DAMAGED,
        UNDAMAGED
    }
    
    public enum YardType
    {
        YARD_REPAIR,
        YARD_OIL,
        YARD_CONTAINER
    }
    
    public enum Direction
    {
        NORTH,
        WEST,
        SOUTH,
        EAST
    }
}