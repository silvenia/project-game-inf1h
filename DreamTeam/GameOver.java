import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.*;  
import java.awt.Color;  
import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Screen that is used in all the minigames to show the player that he/she is gameOver and the score he obtained
 */
public class GameOver extends World
{

    /**
     * Constructor: sets background, adds quit button, draws the score and calls the save function
     * 
     */
    public GameOver(String game, int score)
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1280, 720, 1); 
        setBackground("GameOver.png");

        QuitButton quit = new QuitButton();
        quit.setImage("Menu/PauseMenu/QuitButton.png");   
        addObject(quit, 650, 550);

        updateScore(score);
        saveGame(game, score);
    }

    /**
     * draws the score to the screen
     */
    private void updateScore(int score)
    {
        GreenfootImage image = getBackground();  
        image.setColor(new Color(78, 142, 222));  
        image.setFont(new Font("Cooper Black", Font.BOLD, 100));
        image.drawString(Integer.toString(score), 620, 400); 
    }

    /**
     * saves the game to save.txt
     * save.txt is in the following format:
     * DAY/MONTH/YEAR | MINIGAME: SCORE
     */
    public void saveGame(String game, int scored){
        java.util.Date date= new java.util.Date();
        try {  
            BufferedWriter bw = new BufferedWriter(new FileWriter("save.txt", true));  
            bw.write(new SimpleDateFormat("dd/MM/yyyy").format(date.getTime()) + " | " + game + ": " + Integer.toString(scored));  
            bw.newLine();  
            bw.close();  
        } catch (Exception e) {  
            System.err.println("Error: "+e.getMessage());  
        }  

    }
}

