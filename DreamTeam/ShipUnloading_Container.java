import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)\
import java.util.List;

/**
 * Write a description of class Container here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ShipUnloading_Container extends Actor{

    private int weight;
    
    int counter = 0;
    
    ShipUnloading_CranePlay crane;
    
    public ShipUnloading_Container(){
        turn(-90);
        setContainerState();
    }

    /**
     * Act - do whatever the Container wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act(){
        if(crane != null)
            setLocation(crane.getX(), crane.getY());      
    } 
        
    public void setContainerState(){
        int decider = Greenfoot.getRandomNumber(101);
        
        if(decider < 50){
            weight = 350;
            setImage("Minigame 2/Container.png");
        }
        
        if(decider >= 50){
            weight = 500;
            setImage("Minigame 2/Container_heavy.png");
        }
    }
    
    public ShipUnloading_CranePlay getCrane(){
        return crane;
    }
    
    public void setCrane(ShipUnloading_CranePlay crane){
        ShipUnloading_ContainerPosition containerPosition = (ShipUnloading_ContainerPosition)getOneIntersectingObject(ShipUnloading_ContainerPosition.class);        
        if(containerPosition != null)
            containerPosition.removeContainer();
            
        this.crane = crane;
    }    
    
    public int getWeight(){
        return weight;
    }
}
