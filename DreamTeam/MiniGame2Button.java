import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MiniGame2Button here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MiniGame2Button extends MenuActors
{
    /**
     * Act - do whatever the MiniGame2Button wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        mouseHover();
        if(Greenfoot.mouseClicked(this))
        {
             World nextWorld = new GameStart(GameStart.GameType.SHIPUNLOADING);  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld);  
            int x = getWorld().getWidth()/2;  
            int y = getWorld().getHeight()/2;  
            getWorld().addObject(fader, x, y); 
        }        
    }    
    
    public void setHoverImage()
    {
        setImage("Menu/GameMenu/MiniGame2-hovered.png");
    }
    
    public void setDefaultImage()
    {
        setImage("Menu/GameMenu/MiniGame2.png");
    }  
}
