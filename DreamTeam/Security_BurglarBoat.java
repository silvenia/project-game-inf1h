import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Write a description of class BurglarBoat here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Security_BurglarBoat extends SmoothMover
{
    /**
     * Act - do whatever the BurglarBoat wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    final SoundEffect sound = new SoundEffect();
    private Security world;
    
    private boolean droppedBurglar = false;
    private boolean burglarAboard = true;
    private boolean leavingBehind = false;
    private int dropPoint;
    private Timer waitTimer;
    public Security_BurglarBoat(){
        setImage("Minigame 4/BurglarBoat.png");
        Random random = new Random();
        this.dropPoint = random.nextInt(760 - 550) + 550;
        //System.out.println(this.dropPoint);
    }
    
    public void addedToWorld(World world) {
        this.world = (Security) world;    
        sound.playEffect(SoundEffect.Effect.SAILING_BOAT, false);  
    }
    
    public void act() 
    {        
        if(getX() == dropPoint && (burglarAboard == false  || droppedBurglar == false) && leavingBehind == false)
        {
            move(0);
            if(!droppedBurglar)
            {
                droppedBurglar = true;
                burglarAboard = false;               
                
                int spawnPosition = Greenfoot.getRandomNumber(2) + 6;
                int finishPosition = Greenfoot.getRandomNumber(9);
                
                getWorld().addObject(new Security_Burglar(world.getStartingPoint().get(spawnPosition), world.getFinishPoint().get(finishPosition)), world.getStartingPoint().get(spawnPosition).getX(), world.getStartingPoint().get(spawnPosition).getY());
                
                leaveBehind();
                sound.stopEffect();
           }
            
        }            
        else
            move(1.2);
            
        if(isAtEdge())
        {
            if(burglarAboard == true)
            {
               Security security = (Security)getWorld();
               security.reduceSatisfaction(15);
               if(security.getSatisfaction() < 50)
               security.gameOver();
            }
            sound.stopEffect();  
            world.removeBoat();
            getWorld().removeObject(this);    
        }
        // Add your action code here.
    }    
    
    public boolean isLoaded () {
        return burglarAboard;
    }
    
    public boolean loadBurglar(){
        if(!isLoaded())
        {
            burglarAboard = true;
            return true;
        }
            
        return false;
    }
    
    private void leaveBehind (){
     if (waitTimer != null)
        return;
        final GameObject_CustomTimer timer = new GameObject_CustomTimer(null);
        getWorld().addObject(timer, getX() + 50, getY());    
        timer.start(10.0d);
        ActionListener taskPerformer = new ActionListener() 
            {
                public void actionPerformed(ActionEvent evt) {
                    timer.substractTime(0.1d);
                    final double timePercent = 10.0d;
                    if(timer.getTimeLeft() < ( timePercent * 0.1 ))
                    {
                        timer.setImage("Timer-4.png");
                                           }
                    else if(timer.getTimeLeft() < ( timePercent * 0.5 ))
                    {
                        timer.setImage("Timer-3.png");
                    }
                    else if(timer.getTimeLeft() < ( timePercent * 0.75 ))
                    {
                        timer.setImage("Timer-2.png");
                    }
                    
                    if(timer.getTimeLeft() <= 0 || burglarAboard)
                    {   
                        waitTimer.stop();
                        timer.removeTimer();
                        leavingBehind = true;
                        sound.playEffect(SoundEffect.Effect.SAILING_BOAT, false);  
                    }                
                }
            };
            
            waitTimer = new Timer(100, taskPerformer);   
            waitTimer.setInitialDelay(200);
            waitTimer.start();
    }
}
