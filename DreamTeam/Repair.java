import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;  

/**
 * Minigame 3: Repair
 */
public class Repair extends World
{
    private int timer = 0;
    public int fundsReparateur = 0;
    private int scoreReparateur;
    public GameObject_Scores score = new GameObject_Scores(new Color(0,0,0));
    public GameObject_Funds funding = new GameObject_Funds(new Color(0,0,0));
    private int counter = 0;
    public int containersLetThrough = 0;
    private final int MAX_CONTAINERS = 3;
    private Sound sound = Sound.getInstance();
    public boolean screwBought, drillBought = false;
    /**
     * Constructor: places background, plays music, adds HUD components
     * 
     */
    public Repair(){    
        super(1280, 720, 1); 

        setBackground("Minigame 3/backgroundReparateur.png");
        prepare(); 
        sound.setMusic(Sound.MusicTrack.GAME3);

        GameObject_Clock clock = new GameObject_Clock("Time: ", new Color(0,0,0));

        addObject (funding, 70,17);
        funding.increaseFunds(100000);
        addObject (score, 640, 17);
        addObject (clock, 1220, 17);
        setPaintOrder(GameObject_TextFloat.class, GameObject_TextTimer.class, GameObject_CustomTimer.class,Repair_Player.class, Repair_Tools.class, Repair_Ekey.class, Repair_BrokenContainer.class);
    }

    /**
     * Spawns a new container whenever the timer exceeds the MaxTimer count.
     * 
     */
    public void act(){
        int spawnSpeedTimer = 0;
        int maxTimer = 1000;
        spawnSpeedTimer++;
        if(spawnSpeedTimer >= 5000){
            maxTimer = 800;
        }

        if(spawnSpeedTimer >= 10000){
            maxTimer = 600;
        }

        timer++;
        if(timer >= maxTimer){
            addObject(new Repair_BrokenContainer(), 9, 182);
            timer = 0;
        }
    }

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare() {

        Repair_Tools wrench = new Repair_Tools(Repair_Tools.ToolType.WRENCH);
        addObject(wrench, 150, 627);
        Repair_Tools brush = new Repair_Tools(Repair_Tools.ToolType.BRUSH);
        addObject(brush, 50, 627);
        Repair_BrokenContainer brokencontainer = new Repair_BrokenContainer();
        addObject(brokencontainer, 9, 182);
        Repair_Player repairplayer = new Repair_Player(wrench, brush); 

        addObject(repairplayer, 344, 302);
        Repair_BuyMenu buymenu = new Repair_BuyMenu();
        addObject(buymenu, 1224, 55);
        Repair_ManualIcon manual = new Repair_ManualIcon();
        addObject(manual, 1198, 653);
    }

    /**
     * Updates the ContainersLetThrough variable because of a container that is not repaired by the player
     * 
     */
    public void updateContainersLetThrough(){
        containersLetThrough++;
        gameOver();
    }
    /**
     * If ContainersLetTrough exceeds MAX_CONTAINERS the GameOver function will trigger which will end the game.
     */
    public void gameOver(){
        if(containersLetThrough == MAX_CONTAINERS){
            Greenfoot.setWorld(new GameOver("Repair", score.getScore()));
        }
    }
}