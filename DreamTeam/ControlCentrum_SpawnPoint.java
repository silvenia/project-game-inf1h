import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Spawnpoint from where the ships spawn 
 */
public class ControlCentrum_SpawnPoint extends Actor
{
    private Timer spawnTimer;
    private int spawnSpeed;
    private int spawnDelay;
    
    /**
     * Constructor: retrieves parameter data, sets image (image = invisible)
     */
    public ControlCentrum_SpawnPoint(int spawnSpeed, int spawnDelay)
    {                 
        this.spawnSpeed = spawnSpeed;
        this.spawnDelay = spawnDelay;
        setImage("Minigame 1/SpawnPoint.png");
    }
    
    /**
     * Execute the spawnship method after the object is added to the world.
     */
    protected void addedToWorld(World world)  
    {
        spawnShip((ControlCentrum)world);
    }
    
    /**
     * Check if this spawnpoint is currently spawning ships.
     */
    public boolean isSpawning()
    {
        if(spawnTimer == null)
            return false;
            
        return true;
    }
    
    /**
     * This method checks whether there is a ship in close proximity,
     * returns true if there are no ships in range and false if there are.
     */
    public boolean canSpawn()
    {        
        List<ControlCentrum_Ship> shipList = getIntersectingObjects(ControlCentrum_Ship.class); 
        if(shipList.size() > 0)
            return false;
            
        return true;
    }
    
    /**
     * This method spawns the ships using the timers derived from,
     * spawnSpeed and spawnDelay.
     */
    public void spawnShip(final ControlCentrum c)
    {
        if(spawnTimer != null)
            return;

        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {

                //50% spawn chance of oil and container type ships
                int shipDecider = Greenfoot.getRandomNumber(10);
                int shipSize = Greenfoot.getRandomNumber(15);                
                int repairDecider = Greenfoot.getRandomNumber(10);
                
                int repairTime = 0;
                Enum.ShipType type = null;
                Enum.ShipState shipState = Enum.ShipState.UNDAMAGED;  
                
                if(shipDecider < 5){      
                    if(shipSize <= 5)
                        type = Enum.ShipType.OIL_SMALL;
                    else if(shipSize <= 10)
                        type = Enum.ShipType.OIL_MEDIUM;
                    else
                        type = Enum.ShipType.OIL_LARGE;
                        
                }
                else if(shipDecider >= 5){
                    if(shipSize <= 5)
                        type = Enum.ShipType.CONTAINER_SMALL;
                    else if(shipSize <= 10)
                        type = Enum.ShipType.CONTAINER_MEDIUM;
                    else 
                        type = Enum.ShipType.CONTAINER_LARGE;
                }
        
                //30% chance for a spawned ship to be damaged
                if(repairDecider < 3){
                    repairTime = 100;
                    shipState = Enum.ShipState.DAMAGED;
                }            
                else if(repairDecider >= 3){
                    repairTime = 0;
                    shipState = Enum.ShipState.UNDAMAGED;
                }
                    
                if(canSpawn())
                {
                    c.addObject(new ControlCentrum_Ship(type, shipState), getX(), getY());    
                }
            }
        };
        
        spawnTimer = new Timer(spawnSpeed, taskPerformer);   
        spawnTimer.setInitialDelay(spawnDelay);
        spawnTimer.start();
    }
    
    /**
     * This method changed the spawnSpeed.
     */
    public void setSpawnSpeed(int speed)
    {
        if(spawnTimer == null)
            return;
            
        spawnTimer.setDelay(speed);
    }            
}
