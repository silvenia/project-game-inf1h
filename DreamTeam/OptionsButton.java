import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class OptionsButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class OptionsButton extends MenuActors
{
    /**
     * Act - do whatever the OptionsButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        mouseHover();
        if(Greenfoot.mouseClicked(this))
        {            
            World nextWorld = new OptionsMenu(getWorld()); // create using name of your new world  
            MenuObject_Fader fader = new MenuObject_Fader(nextWorld); // create curtain object  
            int x = getWorld().getWidth()/2;  
            int y = getWorld().getHeight()/2;  
            getWorld().addObject(fader, x, y); // add curtain into world              
        }
    }    
    
    public void setHoverImage()
    {
        setImage("Menu/MainMenu/OptionsButton-hovered.png");
    }
    
    public void setDefaultImage()
    {
        setImage("Menu/MainMenu/OptionsButton.png");
    }    
}
