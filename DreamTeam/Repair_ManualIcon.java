import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Manual here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Repair_ManualIcon extends GameButtons
{
    /**
     * Act - do whatever the Manual wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    Repair_Manual repairManual = new Repair_Manual();
    private int timesClicked = 0;
    
    public Repair_ManualIcon(){
        setImage("Minigame 3/Manual.png");
    }
    
    public void act() {
        Repair world = (Repair) getWorld();
        if(Greenfoot.mouseClicked(this)){
            timesClicked++;
            if(timesClicked % 2 == 1)
                world.addObject(repairManual, this.getX() - 100, this.getY() - 250); 
                
            if(timesClicked % 2 == 0)
                world.removeObject(repairManual);
        }
    }    
}
