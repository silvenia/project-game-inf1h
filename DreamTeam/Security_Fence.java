import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Fence object
 */
public class Security_Fence extends Actor
{
    /**
     * gives the enum the corrosponding fence image
     */
    public Security_Fence(Enum.FenceType fenceType){
        switch(fenceType){
            case LEFTUP:
            case RIGHTUP:{
                setImage("Minigame 4/left_up.png");
                break;
            }
            case LEFTUP_SIDE:{
                setImage("Minigame 4/left_top_side.png");
                break;
            }
            case RIGHTUP_SIDE:{
                setImage("Minigame 4/right_top_side.png");
                break;
            }
            case LEFTBOTTOM_SIDE:{
                setImage("Minigame 4/left_bottom_side.png");
                break;
            }
            case RIGHTBOTTOM_SIDE:{
                setImage("Minigame 4/right_bottom_side.png");
                break;
            }            
        }
    }
}
