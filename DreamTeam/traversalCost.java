/*
    This is an exapmle of an A* Pathfinding Algorithm written in Java/Greenfoot for my educational purposes and to prototype for future useage in games
    Copyright (C) 2011 Shaun Steenkamp

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
/**
 * Stores connected nodes and the cost to get to them.
 * @author Shaun S.
 * @version 0.5
 */
public class traversalCost  
{
    private Security_PathNode node;
    private double cost;
    
    public traversalCost(Security_PathNode node, double cost) {
        this.node = node;
        this.cost = cost;
    }
    
    public Security_PathNode getNode() { return node; }
    public double getCost() { return cost; }
}