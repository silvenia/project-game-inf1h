/**
 * This class stores the x and y coordinates
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Coord  
{
    // instance variables - replace the example below with your own
    public int x;
    public int y;

    /**
     * Constructor for objects of class Coord
     */
    public Coord(final int x, final int y)
    {
        this.x = x;
        this.y = y;
    }
}